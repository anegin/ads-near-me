# AdsNearMe - Android App #

Add and search Ads placed on iBeacon/Eddystone beacons

### Development environment ###

* [Android Studio 3.0 RC 2](https://developer.android.com/sdk/installing/studio.html)
* [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Android SDK](http://developer.android.com/sdk/)
* * Build-tools 26.0.2
* * Min API 16, target API 26

### Gradle plugins ###

* [Dexcount Gradle Plugin 0.8.1](https://github.com/KeepSafe/dexcount-gradle-plugin) ([Apache License 2.0](https://github.com/KeepSafe/dexcount-gradle-plugin/blob/master/LICENSE))

### Dependencies ###

* Support libraries 26.1.0 (design)
* Android Architecture Components 1.0.0-rc1 (LiveData, Room)
* Google Play Services 11.4.2 (Location)
* Firebase 11.4.2 (Cloud Messaging)

### Third-party libraries (from repository) ###

* [OkHttp 3.9.0](http://square.github.io/okhttp) ([Apache License 2.0](http://square.github.io/okhttp/#license))
* [GSON 2.8.2](https://code.google.com/p/google-gson) ([Apache License 2.0](https://github.com/google/gson/blob/master/LICENSE))
* [Dagger 2.12](https://github.com/google/dagger) ([Apache License 2.0](https://github.com/google/dagger/blob/master/LICENSE.txt))
* [ButterKnife 8.8.1](https://github.com/JakeWharton/butterknife) ([Apache License 2.0](https://github.com/JakeWharton/butterknife/blob/master/LICENSE.txt))
* [RxJava 2.1.5](https://github.com/ReactiveX/RxJava) ([Apache License 2.0](https://github.com/ReactiveX/RxJava/blob/2.x/LICENSE))
* [RxAndroid 2.0.1](https://github.com/ReactiveX/RxAndroid) ([Apache License 2.0](https://github.com/ReactiveX/RxJava/blob/2.x/LICENSE))
* [OkHttp 3.9.0](https://github.com/square/okhttp) ([Apache License 2.0](https://github.com/square/okhttp/blob/master/LICENSE.txt))
* [Glide 4.2.0](https://github.com/bumptech/glide) ([Apache License 2.0](https://github.com/bumptech/glide/blob/master/LICENSE))
* [Joda-Time-Android 2.9.9](https://github.com/dlew/joda-time-android) ([Apache License 2.0](https://github.com/dlew/joda-time-android/blob/master/LICENSE))
* [Facebook Android SDK (Login) 4.27.0](https://github.com/facebook/facebook-android-sdk) ([License](https://github.com/facebook/facebook-android-sdk/blob/master/LICENSE.txt))
