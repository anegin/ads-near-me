package com.adsnearme.app.api;

import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.model.api.request.GetAppInfoRequest;
import com.adsnearme.app.model.api.request.GetBeaconAdsRequest;
import com.adsnearme.app.model.api.request.GetUserBeaconsRequest;
import com.adsnearme.app.model.api.request.GetUserInfoRequest;
import com.adsnearme.app.model.api.request.RegUserRequest;
import com.adsnearme.app.model.api.request.ToggleAdFavoriteRequest;
import com.adsnearme.app.model.api.request.UpdateBeaconRequest;
import com.adsnearme.app.model.api.response.GetAppInfoResponse;
import com.adsnearme.app.model.api.response.GetBeaconAdsResponse;
import com.adsnearme.app.model.api.response.GetUserBeaconsResponse;
import com.adsnearme.app.model.api.response.GetUserInfoResponse;
import com.adsnearme.app.model.api.response.RegUserResponse;
import com.adsnearme.app.model.api.response.ToggleAdFavoriteResponse;
import com.adsnearme.app.model.api.response.UpdateBeaconResponse;

import io.reactivex.Observable;

public class Api {

	static final String API_URL = "http://meshok.myjino.ru/server.php";

	public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static Observable<GetUserInfoResponse> getUserInfo(GetUserInfoRequest request) {
		return ApiHelper.enqueue(request, GetUserInfoResponse.class);
	}

	public static Observable<RegUserResponse> registerUser(RegUserRequest request) {
		return ApiHelper.enqueue(request, RegUserResponse.class);
	}

	public static Observable<GetUserBeaconsResponse> getUserBeacons() {
		return ApiHelper.enqueue(new GetUserBeaconsRequest(), GetUserBeaconsResponse.class);
	}

	public static Observable<GetBeaconAdsResponse> getBeaconAds(Beacon beacon) {
		return ApiHelper.enqueue(new GetBeaconAdsRequest(beacon), GetBeaconAdsResponse.class);
	}

	public static Observable<UpdateBeaconResponse> updateBeacon(UpdateBeaconRequest request) {
		return ApiHelper.enqueue(request, UpdateBeaconResponse.class);
	}

	public static Observable<GetAppInfoResponse> getAppInfo() {
		return ApiHelper.enqueue(new GetAppInfoRequest(), GetAppInfoResponse.class);
	}

	public static Observable<ToggleAdFavoriteResponse> toggleAdFavorite(String adId, boolean isFavorite) {
		return ApiHelper.enqueue(new ToggleAdFavoriteRequest(adId, isFavorite), ToggleAdFavoriteResponse.class);
	}

}