package com.adsnearme.app.api;

import android.text.TextUtils;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.model.AuthData;
import com.adsnearme.app.auth.AuthType;
import com.adsnearme.app.model.api.request.ApiRequest;
import com.adsnearme.app.model.api.response.ApiException;
import com.adsnearme.app.model.api.response.ApiResponse;
import com.adsnearme.app.utils.GsonUtils;
import com.adsnearme.app.utils.network.Network;
import com.adsnearme.app.utils.network.NoNetworkException;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class ApiHelper {

	static <T extends ApiResponse> Observable<T> enqueue(ApiRequest apiRequest, Class<T> responseClass) {
		Observable<T> observable = Observable.defer(() -> Observable.just(execute(apiRequest, responseClass)));
		return observable.subscribeOn(Schedulers.io());
	}

	public static <T extends ApiResponse> T execute(ApiRequest apiRequest, Class<T> responseClass) throws IOException, ApiException {
		try {
			if (!Network.isNetworkAvailable())
				throw new NoNetworkException();

			FormBody.Builder formBody = new FormBody.Builder();
			formBody.add("v_android", "1");
			addAuthorizationData(formBody);

			Map<String, String> requestMap = GsonUtils.objectToMap(apiRequest);
			for (String key : requestMap.keySet())
				formBody.add(key, requestMap.get(key));

			Request request = new Request.Builder()
					.url(Api.API_URL)
					.post(formBody.build())
					.build();

			OkHttpClient okHttpClient = Network.getOkHttpClient();
			Response response = okHttpClient.newCall(request).execute();

			if (!response.isSuccessful())
				throw new IOException("Error: HTTP " + response.code() + " " + response.message());

			ResponseBody responseBody = response.body();
			if (responseBody == null)
				throw new IOException("Empty response body");

			T parsedResponse;
			try {
				parsedResponse = GsonUtils.GSON.fromJson(responseBody.charStream(), responseClass);
				responseBody.close();
			} catch (JsonSyntaxException e) {
				throw new IOException("Can't parse server response");
			}

			if (parsedResponse.answerType != null) {
				switch (parsedResponse.answerType) {
					case SUCCESS:
						return parsedResponse;
					case ERROR:
						throw new ApiException(parsedResponse.answer);
				}
			}

			throw new IOException("Unknown response answer_type");

		} catch (IOException | ApiException e) {
			e.printStackTrace();
			throw e;
		}
	}

	private static void addAuthorizationData(FormBody.Builder formBody) {
		AuthData authData = AdsApp.component().auth().getAuthData();
		if (!TextUtils.isEmpty(authData.sessionId) && !TextUtils.isEmpty(authData.sessionsName)) {
			formBody.add(authData.sessionsName, authData.sessionId);
		}
		if (authData.authType != null) {
			switch (authData.authType) {
				case FACEBOOK:
					formBody
							.add("from", AuthType.FACEBOOK.value)
							.add("fb_user_id", authData.socialUserId)
							.add("access_token", authData.socialAccessToken);
					break;
				case VKONTAKTE:
					formBody
							.add("from", AuthType.VKONTAKTE.value)
							.add("vk_user_id", authData.socialUserId)
							.add("access_token", authData.socialAccessToken);
					break;
			}
		}
	}

}
