package com.adsnearme.app.firebase;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MessagingService extends FirebaseMessagingService {

	private static final String TAG = MessagingService.class.getSimpleName();

	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		Log.d(TAG, "Cloud Message received from: " + remoteMessage.getFrom());

		if (remoteMessage.getData().size() > 0) {
			Log.d(TAG, "Message data payload: " + remoteMessage.getData());
			/*
			if (isLongRunningOperation_MoreThan10Seconds) {
				scheduleJob();  // Firebase Job Dispatcher.
			} else {
				handleNow();
			}
			*/
		}

		if (remoteMessage.getNotification() != null) {
			Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
		}
	}

}