package com.adsnearme.app.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.R;
import com.adsnearme.app.model.AuthData;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartActivity extends AppCompatActivity {

	@BindView(R.id.image_logo)
	ImageView imageLogo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		ButterKnife.bind(this);

		imageLogo.setAlpha(0f);
		imageLogo.animate()
				.alpha(1f)
				.setDuration(1000)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						continueStart();
					}
				})
				.start();
	}

	private void continueStart() {
		if (isFinishing()) return;

		AuthData authData = AdsApp.component().auth().getAuthData();
		if (authData.authType == null) {
			startActivity(new Intent(this, SignInActivity.class));
		} else {
			startActivity(new Intent(this, MainActivity.class));
		}
		finish();
		overridePendingTransition(0, 0);
	}

}