package com.adsnearme.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.R;
import com.adsnearme.app.api.Api;
import com.adsnearme.app.model.AuthData;
import com.adsnearme.app.auth.AuthType;
import com.adsnearme.app.model.api.request.GetUserInfoRequest;
import com.adsnearme.app.model.api.response.ApiException;
import com.adsnearme.app.model.api.response.GetUserInfoResponse;
import com.adsnearme.app.utils.network.NoNetworkException;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

import io.reactivex.android.schedulers.AndroidSchedulers;

public abstract class BaseLoginActivity extends AppCompatActivity {

	private static final String TAG = BaseLoginActivity.class.getSimpleName();

	public abstract void showProgress();

	public abstract void hideProgress();

	private CallbackManager callbackManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		callbackManager = CallbackManager.Factory.create();
		LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult loginResult) {
				requestFacebookUserInfo(loginResult.getAccessToken());
			}

			@Override
			public void onCancel() {
				Log.v(TAG, "Facebook login canceled");
				hideProgress();
			}

			@Override
			public void onError(FacebookException e) {
				Log.v(TAG, "Facebook login failed", e);
				hideProgress();
				Toast.makeText(BaseLoginActivity.this, R.string.facebook_login_failed, Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	protected void loginWithFacebook() {
		LoginManager.getInstance().logOut();
		LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
	}

	private void requestFacebookUserInfo(AccessToken accessToken) {
		GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
			String firstName = null;
			String lastName = null;
			if (response.getError() == null && object != null) {
				firstName = object.optString("first_name");
				lastName = object.optString("last_name");
			}
			onFacebookLoginSuccess(accessToken, firstName, lastName);
		});
		Bundle params = new Bundle();
		params.putString("fields", "first_name,last_name,email");
		request.setParameters(params);
		request.executeAsync();
	}

	private void onFacebookLoginSuccess(AccessToken accessToken, String name, String surname) {
		AuthData authData = new AuthData(AuthType.FACEBOOK);
		authData.socialUserId = accessToken.getUserId();
		authData.socialAccessToken = accessToken.getToken();
		AdsApp.component().auth().setAuthData(authData);

		GetUserInfoRequest request = new GetUserInfoRequest();
		request.name = name;
		request.surname = surname;

		Api.getUserInfo(request)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(response -> onUserLoginSuccess(response, authData), this::onUserLoginError);
	}

	protected void onUserLoginSuccess(GetUserInfoResponse response, AuthData authData) {
		if (authData == null) {
			authData = new AuthData(AuthType.EMAIL);
		}
		authData.sessionId = response.sessionId;
		authData.sessionsName = response.sessionName;
		authData.user = response.answer.getUser();
		AdsApp.component().auth().setAuthData(authData);

		Toast.makeText(this, R.string.login_success, Toast.LENGTH_SHORT).show();
		startActivity(new Intent(this, MainActivity.class));
		finish();
	}

	protected void onUserLoginError(Throwable t) {
		hideProgress();
		if (t instanceof ApiException) {
			ApiException apiException = (ApiException) t;
			if (ApiException.USER_NOT_EXISTS.equals(apiException.code)) {
				Toast.makeText(this, R.string.error_user_nor_exists, Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this, R.string.login_failed, Toast.LENGTH_SHORT).show();
			}
		} else if (t instanceof NoNetworkException) {
			Toast.makeText(this, R.string.no_network_connection, Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, R.string.communication_error_with_server, Toast.LENGTH_SHORT).show();
		}
	}

}