package com.adsnearme.app.ui.fragment;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.adsnearme.app.R;
import com.adsnearme.app.api.Api;
import com.adsnearme.app.arch.viewmodel.AdsListViewModel;
import com.adsnearme.app.arch.viewmodel.LocationViewModel;
import com.adsnearme.app.geofence.UpdateGeofencesJob;
import com.adsnearme.app.model.api.Ad;
import com.adsnearme.app.model.api.request.GetAdListRequest;
import com.adsnearme.app.ui.adapter.AdsListAdapter;
import com.adsnearme.app.ui.adapter.viewholder.AdViewHolder;
import com.adsnearme.app.utils.LocationHelper;
import com.adsnearme.app.utils.Utils;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class AdsListFragment extends Fragment implements AdViewHolder.Listener, SwipeRefreshLayout.OnRefreshListener, OnMapReadyCallback {

	@BindView(R.id.tab_layout)
	TabLayout tabLayout;
	@BindView(R.id.swipe_refresh_layout)
	SwipeRefreshLayout swipeRefreshLayout;
	@BindView(R.id.recyclerview_ads)
	RecyclerView recyclerView;
	@BindView(R.id.text_error)
	TextView textError;
	@BindView(R.id.text_no_ads)
	TextView textNoAds;
	@BindView(R.id.text_radius)
	TextView textRadius;
	@BindView(R.id.button_radius_minus)
	Button buttonRadiusMinus;
	@BindView(R.id.button_radius_plus)
	Button buttonRadiusPlus;
	@BindView(R.id.layout_list)
	View layoutList;
	@BindView(R.id.layout_map_fragment)
	View viewMapFragment;
	@BindView(R.id.layout_bottom)
	View layoutBottom;

	private GoogleMap googleMap;
	private boolean isMapAvailable;

	private Circle radiusCircle;
	private Marker myLocationMarker;
	private final List<Marker> adMarkers = new ArrayList<>();
	private final List<Circle> adCircles = new ArrayList<>();

	private Location myLocation;

	private AdsListViewModel adsListViewModel;

	private AdsListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_ads_list, parent, false);
	}

	@Override
	public void onViewCreated(@NonNull View root, Bundle savedInstanceState) {
		ButterKnife.bind(this, root);

		Activity activity = getActivity();
		assert activity != null;

		adsListViewModel = ViewModelProviders.of(this).get(AdsListViewModel.class);

		adapter = new AdsListAdapter(Glide.with(this), this);
		adapter.setDistanceInMeters(adsListViewModel.isDistanceInMeters());

		recyclerView.setLayoutManager(new LinearLayoutManager(activity));
		recyclerView.addItemDecoration(new DividerItemDecoration(activity, LinearLayoutManager.VERTICAL));
		recyclerView.setAdapter(adapter);

		swipeRefreshLayout.setOnRefreshListener(this);

		buttonRadiusMinus.setOnClickListener(v -> adsListViewModel.decreaseRadius());
		buttonRadiusPlus.setOnClickListener(v -> adsListViewModel.increaseRadius());
		adsListViewModel.getRadiusIndex().observe(this, i -> onRadiusChanged(i != null ? i : 0));

		tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				boolean distanceInMeters = tab.getPosition() == 0;
				adsListViewModel.setDistanceInMeters(distanceInMeters);
				adapter.setDistanceInMeters(distanceInMeters);
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {
			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {
			}
		});

		if (!adsListViewModel.isRequested()) {
			if (myLocation != null) {
				restartLoad();
			} else {
				if (!LocationHelper.hasPermission()) {
					textError.setVisibility(View.VISIBLE);
					textNoAds.setVisibility(View.GONE);
					if (swipeRefreshLayout.isRefreshing()) {
						swipeRefreshLayout.setRefreshing(false);
					}
				} else {
					textError.setVisibility(View.GONE);
					textNoAds.setVisibility(View.GONE);
					if (!swipeRefreshLayout.isRefreshing()) {
						swipeRefreshLayout.setRefreshing(true);
					}
				}
			}
		}

		adsListViewModel.getRequest()
				.observe(this, this::updateRadiusCircleOnMap);

		adsListViewModel.getIsMapMode()
				.observe(this, this::onIsMapModeChanged);

		ViewModelProviders.of(getActivity()).get(LocationViewModel.class)
				.getLocation().observe(this, this::onMyLocationChanged);

		root.findViewById(R.id.button_filter).setOnClickListener(v -> UpdateGeofencesJob.startNow());
	}

	@Override
	public void onResume() {
		super.onResume();
		Context context = getContext();
		assert context != null;
		int result = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
		isMapAvailable = result == ConnectionResult.SUCCESS;
		if (isMapAvailable) {
			Fragment mapFragment = getChildFragmentManager().findFragmentByTag("ads_map");
			if (mapFragment == null || !mapFragment.isAdded() || mapFragment.isDetached()) {
				GoogleMapOptions mapOptions = new GoogleMapOptions();
				mapOptions.mapType(GoogleMap.MAP_TYPE_NORMAL);
				mapOptions.liteMode(false);
				mapOptions.mapToolbarEnabled(true);
				mapOptions.compassEnabled(true);
				mapOptions.rotateGesturesEnabled(true);
				mapOptions.scrollGesturesEnabled(true);
				mapOptions.tiltGesturesEnabled(true);
				mapOptions.zoomGesturesEnabled(true);
				mapOptions.zoomControlsEnabled(true);
				mapFragment = SupportMapFragment.newInstance(mapOptions);
				((SupportMapFragment) mapFragment).getMapAsync(this);
				getChildFragmentManager()
						.beginTransaction()
						.add(R.id.layout_map_fragment, mapFragment, "ads_map")
						.commit();
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
		menuInflater.inflate(R.menu.fragment_ads_list, menu);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		MenuItem mode = menu.findItem(R.id.action_ads_mode);
		if (isMapAvailable) {
			mode.setVisible(true);
			Boolean mapMode = adsListViewModel.getIsMapMode().getValue();
			mode.setIcon(mapMode != null && mapMode ? R.drawable.ic_action_list : R.drawable.ic_action_map);
			mode.setTitle(mapMode != null && mapMode ? R.string.action_ads_list : R.string.action_ads_map);
		} else {
			mode.setVisible(false);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_ads_mode:
				adsListViewModel.toggleIsMapMode();
				return true;
		}
		return false;
	}

	@Override
	public void onRefresh() {
		restartLoad();
	}

	private void onMyLocationChanged(Location location) {
		this.myLocation = location;
		if (myLocation != null) {
			if (!adsListViewModel.isRequested()) {
				restartLoad();
			}
			adapter.setMyLocation(myLocation);
		}
	}

	private void onIsMapModeChanged(Boolean isMapMode) {
		if (isMapMode != null && isMapMode) {
			viewMapFragment.setVisibility(View.VISIBLE);
			tabLayout.setVisibility(View.INVISIBLE);
			layoutList.setVisibility(View.INVISIBLE);

			layoutBottom.setBackgroundColor(0xcc395962);

		} else {
			viewMapFragment.setVisibility(View.INVISIBLE);
			tabLayout.setVisibility(View.VISIBLE);
			layoutList.setVisibility(View.VISIBLE);

			layoutBottom.setBackgroundColor(0x1affffff);
		}
		Activity activity = getActivity();
		if (activity != null) activity.invalidateOptionsMenu();
	}

	private void onRadiusChanged(int radiusIndex) {
		double radiusMeters = AdsListViewModel.RADIUSES[radiusIndex];
		textRadius.setText(LocationHelper.distanceToString(radiusMeters));
		buttonRadiusMinus.setEnabled(radiusIndex > 0);
		buttonRadiusPlus.setEnabled(radiusIndex + 1 < AdsListViewModel.RADIUSES.length);

		postDelayedReload();
	}

	// =============

	private Handler restartLoadHandler = new Handler();
	private Runnable restartLoadRunnable = this::restartLoad;

	private void postDelayedReload() {
		restartLoadHandler.removeCallbacks(restartLoadRunnable);
		restartLoadHandler.postDelayed(restartLoadRunnable, 300);
	}

	private void restartLoad() {
		if (myLocation == null) return;
		restartLoadHandler.removeCallbacks(restartLoadRunnable);

		textNoAds.setVisibility(View.GONE);
		textError.setVisibility(View.GONE);
		if (!swipeRefreshLayout.isRefreshing()) {
			swipeRefreshLayout.setRefreshing(true);
		}

		GetAdListRequest request = new GetAdListRequest();
		request.latitude = myLocation.getLatitude();
		request.longitude = myLocation.getLongitude();
		request.radius = adsListViewModel.getRadius();

		adsListViewModel.removeObserver(adsObserver);
		adsListViewModel.setRequest(request);
		adsListViewModel.getAds().observe(this, adsObserver);
	}

	private Observer<PagedList<Ad>> adsObserver = new Observer<PagedList<Ad>>() {
		@Override
		public void onChanged(@Nullable PagedList<Ad> ads) {
			if (ads != null) {
				adapter.setList(ads);
				updateMarkersOnMap();
			}
			textNoAds.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
			if (swipeRefreshLayout.isRefreshing()) {
				swipeRefreshLayout.setRefreshing(false);
			}
			textNoAds.setVisibility(View.GONE);
			textError.setVisibility(View.GONE);
		}
	};

	// =============

	@Override
	public void onToggleAdFavorite(Ad ad) {
		if (ad.inFavorites == 1) {
			Api.toggleAdFavorite(ad.id, false)
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(
							r -> {
								if (adsListViewModel.toggleAdFavorite(ad.id, false)) {
									adapter.notifyAdUpdated(ad.id);
								}
							},
							t -> Log.w("ABC", "Can't remove from favorites", t));
		} else {
			Api.toggleAdFavorite(ad.id, true)
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(
							r -> {
								if (adsListViewModel.toggleAdFavorite(ad.id, true)) {
									adapter.notifyAdUpdated(ad.id);
								}
							},
							t -> Log.w("ABC", "Can't add to favorites", t));
		}
	}

	@Override
	public void onAdClicked(Ad ad) {

	}

	@Override
	public void onShareAdClicked(Ad ad) {

	}

	// =======================

	@Override
	public void onMapReady(GoogleMap googleMap) {
		this.googleMap = googleMap;

		googleMap.setPadding(0, 0, 0, layoutBottom.getHeight());

		UiSettings uiSettings = googleMap.getUiSettings();
		uiSettings.setAllGesturesEnabled(true);
		uiSettings.setCompassEnabled(true);
		uiSettings.setMapToolbarEnabled(true);
		uiSettings.setZoomControlsEnabled(true);

		if (LocationHelper.hasPermission()) {
			try {
				googleMap.setMyLocationEnabled(true);
				uiSettings.setMyLocationButtonEnabled(true);
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}

		googleMap.setBuildingsEnabled(true);

		googleMap.setOnMyLocationButtonClickListener(() -> {
			GetAdListRequest request = adsListViewModel.getRequest().getValue();
			if (request != null) {
				LatLng center = new LatLng(request.latitude, request.longitude);
				googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(center, 12f));
			}
			return true;
		});

		googleMap.setOnMapLoadedCallback(() -> {
			googleMap.setOnMapLoadedCallback(null);

			updateMarkersOnMap();

			GetAdListRequest request = adsListViewModel.getRequest().getValue();
			if (request != null) {
				updateRadiusCircleOnMap(request);
			}
		});
	}

	private void updateRadiusCircleOnMap(GetAdListRequest request) {
		if (googleMap == null || request == null) return;

		LatLng center = new LatLng(request.latitude, request.longitude);

		if (radiusCircle != null) radiusCircle.remove();
		radiusCircle = googleMap.addCircle(new CircleOptions()
				.center(center)
				.radius(request.radius)
				.strokeColor(Color.LTGRAY)
				.fillColor(Color.TRANSPARENT));

		if (myLocationMarker != null) myLocationMarker.remove();
		myLocationMarker = googleMap.addMarker(new MarkerOptions()
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_me))
				.anchor(0.5f, 25f / 57f)
				.zIndex(1)
				.position(center));

		double headingRadius = Math.sqrt(2) * request.radius;
		LatLng norhtEast = SphericalUtil.computeOffset(center, headingRadius, 45);
		LatLng southWest = SphericalUtil.computeOffset(center, headingRadius, 225);
		LatLngBounds bounds = new LatLngBounds(southWest, norhtEast);

		float padding = Utils.dpToPx(getContext(), 32);
		googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, (int) padding));
	}

	private void updateMarkersOnMap() {
		if (googleMap == null) return;

		for (Marker marker : adMarkers) marker.remove();
		adMarkers.clear();

		for (Circle circle : adCircles) circle.remove();
		adCircles.clear();

		PagedList<Ad> ads = adsListViewModel.getAds().getValue();
		if (ads != null) {
			for (Ad ad : ads) {
				if (ad.latitude != null && ad.longitude != null && ad.latitude != 0 && ad.longitude != 0) {
					LatLng position = new LatLng(ad.latitude, ad.longitude);
					adMarkers.add(googleMap.addMarker(new MarkerOptions()
							.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker))
							.anchor(0.5f, 55f / 66f)
							.title(ad.name)
							.zIndex(2)
							.snippet(ad.description)
							.position(position)
					));
					if (ad.geofenceRadius != null && ad.geofenceRadius > 0) {
						adCircles.add(googleMap.addCircle(new CircleOptions()
								.center(position)
								.radius(ad.geofenceRadius)
								.strokeColor(0xffe77c6e)
								.strokeWidth(1)
								.fillColor(Color.TRANSPARENT)
						));
					}
				}
			}
		}
	}

}