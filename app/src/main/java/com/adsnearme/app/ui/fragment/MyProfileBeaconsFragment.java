package com.adsnearme.app.ui.fragment;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.adsnearme.app.R;
import com.adsnearme.app.arch.viewmodel.MyBeaconsViewModel;
import com.adsnearme.app.model.NoError;
import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.model.api.response.ApiException;
import com.adsnearme.app.ui.activity.ViewBeaconActivity;
import com.adsnearme.app.ui.adapter.MyBeaconsAdapter;
import com.adsnearme.app.ui.adapter.viewholder.BeaconViewHolder;
import com.adsnearme.app.utils.network.NoNetworkException;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyProfileBeaconsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, BeaconViewHolder.Listener {

	private static final String STATE_IS_LOADED_FROM_SERVER = MyProfileBeaconsFragment.class.getName() + ".STATE_IS_LOADED_FROM_SERVER";

	@BindView(R.id.swipe_refresh_layout)
	SwipeRefreshLayout swipeRefreshLayout;
	@BindView(R.id.recyclerview_beacons)
	RecyclerView recyclerViewBeacons;
	@BindView(R.id.text_no_beacons)
	TextView textNoBeacons;

	@BindView(R.id.layout_error)
	View layoutError;
	@BindView(R.id.text_error)
	TextView textError;

	private MyBeaconsAdapter adapter;

	private MyBeaconsViewModel myBeaconsViewModel;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_my_profile_beacons, parent, false);
	}

	@Override
	public void onViewCreated(@NonNull View root, Bundle savedInstanceState) {
		ButterKnife.bind(this, root);

		Context context = getContext();
		assert context != null;

		adapter = new MyBeaconsAdapter(this);

		recyclerViewBeacons.setLayoutManager(new LinearLayoutManager(context));
		recyclerViewBeacons.setAdapter(adapter);
		recyclerViewBeacons.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));

		swipeRefreshLayout.setOnRefreshListener(this);

		myBeaconsViewModel = ViewModelProviders.of(this).get(MyBeaconsViewModel.class);
		myBeaconsViewModel.getBeacons().observe(this, this::onBeaconsListLoaded);
		myBeaconsViewModel.getError().observe(this, this::onError);

		if (savedInstanceState != null) {
			myBeaconsViewModel.setIsLoadedFromServer(savedInstanceState.getBoolean(STATE_IS_LOADED_FROM_SERVER));
		}

		if (!myBeaconsViewModel.isLoadedFromServer()) {
			loadFromServer();
		}
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(STATE_IS_LOADED_FROM_SERVER, myBeaconsViewModel.isLoadedFromServer());
	}

	@Override
	public void onRefresh() {
		loadFromServer();
	}

	@OnClick(R.id.button_error_retry)
	public void loadFromServer() {
		if (!swipeRefreshLayout.isRefreshing()) {
			swipeRefreshLayout.setRefreshing(true);
		}
		layoutError.setVisibility(View.GONE);
		myBeaconsViewModel.loadFromServer();
	}

	private void onBeaconsListLoaded(List<Beacon> beacons) {
		if (swipeRefreshLayout.isRefreshing()) {
			swipeRefreshLayout.setRefreshing(false);
		}
		adapter.setBeacons(beacons);
		textNoBeacons.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
		layoutError.setVisibility(View.GONE);
	}

	private void onError(Throwable err) {
		if (swipeRefreshLayout.isRefreshing()) {
			swipeRefreshLayout.setRefreshing(false);
		}
		if (err instanceof NoError) {
			layoutError.setVisibility(View.GONE);
			return;
		}
		String errorMessage;
		if (err instanceof NoNetworkException) {
			errorMessage = getString(R.string.no_network_connection);
		} else if (err instanceof ApiException) {
			errorMessage = ((ApiException) err).text;
		} else {
			errorMessage = getString(R.string.error_requesting_beacons_list);
		}
		if (adapter.getItemCount() > 0) {
			layoutError.setVisibility(View.GONE);
			Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
		} else {
			layoutError.setVisibility(View.VISIBLE);
			textError.setText(errorMessage);
		}
	}

	@Override
	public void onBeaconClicked(Beacon beacon) {
		Activity activity = getActivity();
		if (activity != null) {
			Intent intent = new Intent(activity, ViewBeaconActivity.class);
			intent.putExtra(ViewBeaconActivity.EXT_BEACON_ID, beacon.id);
			startActivity(intent);
			activity.overridePendingTransition(R.anim.slide_in_activity_start, R.anim.slide_out_activity_start);
		}
	}

}