package com.adsnearme.app.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.R;
import com.adsnearme.app.ui.activity.SignInActivity;

public class MyProfileFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_my_profile, parent, false);
	}

	@Override
	public void onViewCreated(@NonNull View root, Bundle savedInstanceState) {
		MyProfilePagerAdapter pagerAdapter = new MyProfilePagerAdapter(getChildFragmentManager(), getContext());
		ViewPager viewPager = root.findViewById(R.id.view_pager);
		viewPager.setAdapter(pagerAdapter);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
		super.onCreateOptionsMenu(menu, menuInflater);
		menuInflater.inflate(R.menu.fragment_my_profile, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
			case R.id.action_logout:
				confirmLogout();
				return true;
			default:
				return super.onOptionsItemSelected(menuItem);
		}
	}

	private void confirmLogout() {
		Context context = getContext();
		if (context != null) {
			new AlertDialog.Builder(context)
					.setTitle(R.string.confirmation)
					.setMessage(R.string.do_you_want_to_logout)
					.setCancelable(true)
					.setPositiveButton(R.string.yes, (d, w) -> logout())
					.setNegativeButton(R.string.cancel, null)
					.show();
		}
	}

	private void logout() {
		AdsApp.component().auth().setAuthData(null);
		Activity activity = getActivity();
		if (activity != null) {
			activity.startActivity(new Intent(activity, SignInActivity.class));
			activity.finishAffinity();
		}
	}

	// =========================

	private static class MyProfilePagerAdapter extends FragmentStatePagerAdapter {

		private final String titleMyData;
		private final String titleMyBeacons;

		MyProfilePagerAdapter(FragmentManager fm, Context context) {
			super(fm);
			titleMyData = context.getString(R.string.my_data);
			titleMyBeacons = context.getString(R.string.my_beacons);
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
				case 0:
					return new MyProfileDataFragment();
				case 1:
					return new MyProfileBeaconsFragment();
			}
			return null;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case 0:
					return titleMyData;
				case 1:
					return titleMyBeacons;
			}
			return null;
		}
	}

}