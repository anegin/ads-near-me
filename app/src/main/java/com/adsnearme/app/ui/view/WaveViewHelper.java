/*
 *  Copyright (C) 2015, gelitenight(gelitenight@gmail.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.adsnearme.app.ui.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.animation.LinearInterpolator;

import com.adsnearme.app.R;
import com.adsnearme.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * From WaveView library sample
 * https://github.com/gelitenight/WaveView
 */
public class WaveViewHelper {

	private AnimatorSet mAnimatorSet;

	public WaveViewHelper(WaveView waveView) {
		float borderWidth = Utils.dpToPx(waveView.getContext(), 1);
		waveView.setBorder((int) borderWidth, 0xff798e91);
		waveView.setShapeType(WaveView.ShapeType.CIRCLE);
		waveView.setShowWave(true);

		int waveColorFront = ContextCompat.getColor(waveView.getContext(), R.color.app_red);
		int waveColorBack = Color.argb(0x55, Color.red(waveColorFront), Color.green(waveColorFront), Color.blue(waveColorFront));
		waveView.setWaveColor(waveColorBack, waveColorFront);
		waveView.setShowWave(true);

		ObjectAnimator waveShiftAnim = ObjectAnimator.ofFloat(waveView, "waveShiftRatio", 0f, 1f);
		waveShiftAnim.setRepeatCount(ValueAnimator.INFINITE);
		waveShiftAnim.setDuration(2000);
		waveShiftAnim.setInterpolator(new LinearInterpolator());

		ObjectAnimator amplitudeAnim = ObjectAnimator.ofFloat(waveView, "amplitudeRatio", 0.05f, 0.01f);
		amplitudeAnim.setRepeatCount(ValueAnimator.INFINITE);
		amplitudeAnim.setRepeatMode(ValueAnimator.REVERSE);
		amplitudeAnim.setDuration(5000);
		amplitudeAnim.setInterpolator(new LinearInterpolator());

		List<Animator> animators = new ArrayList<>();
		animators.add(waveShiftAnim);
		animators.add(amplitudeAnim);
		mAnimatorSet = new AnimatorSet();
		mAnimatorSet.playTogether(animators);
	}

	public void start() {
		mAnimatorSet.start();
	}

	public void stop() {
		mAnimatorSet.end();
	}

}