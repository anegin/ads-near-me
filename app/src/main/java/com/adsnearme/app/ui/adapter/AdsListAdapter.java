package com.adsnearme.app.ui.adapter;

import android.arch.paging.PagedListAdapter;
import android.location.Location;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.DiffCallback;
import android.view.ViewGroup;

import com.adsnearme.app.model.api.Ad;
import com.adsnearme.app.ui.adapter.viewholder.AdViewHolder;
import com.adsnearme.app.utils.Utils;
import com.bumptech.glide.RequestManager;

public class AdsListAdapter extends PagedListAdapter<Ad, AdViewHolder> {

	private static final int LOCATION_UPDATE_PERIOD = 5000; // 5 сек

	private final RequestManager glide;
	private final AdViewHolder.Listener listener;

	private Location myLocation;
	private long lastLocationUpdateTime;

	private boolean distanceInMeters = true;

	public AdsListAdapter(RequestManager glide, AdViewHolder.Listener listener) {
		super(DIFF_CALLBACK);
		this.glide = glide;
		this.listener = listener;
	}

	public void setMyLocation(Location location) {
		if (this.myLocation == location) return;
		if (this.myLocation != null && location != null
				&& this.myLocation.getLatitude() == location.getLatitude()
				&& this.myLocation.getLongitude() == location.getLongitude()) return;

		this.myLocation = location;

		long now = SystemClock.uptimeMillis();
		if (now - lastLocationUpdateTime > LOCATION_UPDATE_PERIOD) {
			lastLocationUpdateTime = now;
			notifyDataSetChanged();
		}
	}

	public void setDistanceInMeters(boolean distanceInMeters) {
		this.distanceInMeters = distanceInMeters;
		notifyDataSetChanged();
	}

	public synchronized void notifyAdUpdated(String adId) {
		int count = getItemCount();
		for (int i = 0; i < count; i++) {
			Ad ad = getItem(i);
			if (ad != null && Utils.objectEquals(ad.id, adId)) {
				notifyItemChanged(i);
				return;
			}
		}
	}

	@Override
	public AdViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new AdViewHolder(parent, listener);
	}

	@Override
	public void onBindViewHolder(AdViewHolder holder, int position) {
		Ad ad = getItem(position);
		if (ad != null) {
			holder.bind(ad, glide, myLocation, distanceInMeters);
		} else {
			holder.clear();
		}
	}

	private static DiffCallback<Ad> DIFF_CALLBACK = new DiffCallback<Ad>() {
		@Override
		public boolean areItemsTheSame(@NonNull Ad oldItem, @NonNull Ad newItem) {
			return Utils.objectEquals(oldItem.id, newItem.id);
		}

		@Override
		public boolean areContentsTheSame(@NonNull Ad oldItem, @NonNull Ad newItem) {
			return Utils.objectEquals(oldItem.id, newItem.id)
					&& Utils.objectEquals(oldItem.getImageUrl(), newItem.getImageUrl())
					&& Utils.objectEquals(oldItem.name, newItem.name)
					&& Utils.objectEquals(oldItem.distance, newItem.distance)
					&& Utils.objectEquals(oldItem.description, newItem.description)
					&& Utils.objectEquals(oldItem.dateUpdated, newItem.dateUpdated)
					&& Utils.objectEquals(oldItem.dateAdded, newItem.dateAdded)
					&& Utils.objectEquals(oldItem.category, newItem.category)
					&& Utils.objectEquals(oldItem.subCategory, newItem.subCategory)
					&& oldItem.inFavorites == newItem.inFavorites;
		}
	};

}