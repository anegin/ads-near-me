package com.adsnearme.app.ui.adapter.viewholder;

import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.R;
import com.adsnearme.app.model.Category;
import com.adsnearme.app.model.api.Ad;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class BeaconAdViewHolder extends InflaterViewHolder {

	@BindView(R.id.image_ad)
	ImageView imageAd;
	@BindView(R.id.text_title)
	TextView textTitle;
	@BindView(R.id.text_category)
	TextView textCategory;
	@BindView(R.id.text_subcategory)
	TextView textSubcategory;

	private Ad ad;

	public interface Listener {
		void onAdClicked(Ad ad);
	}

	public BeaconAdViewHolder(ViewGroup parent, Listener listener) {
		super(R.layout.listitem_beacon_ad, parent);
		ButterKnife.bind(this, itemView);

		itemView.setOnClickListener(v -> {
			if (listener != null && ad != null) listener.onAdClicked(ad);
		});
	}

	public void bind(Ad ad, RequestManager glide) {
		this.ad = ad;

		// image
		String imageUrl = ad.imagesLittle != null && ad.imagesLittle.size() > 0 ? ad.imagesLittle.get(0) : null;
		if (imageUrl == null || imageUrl.length() == 0) {
			imageUrl = ad.images != null && ad.images.size() > 0 ? ad.images.get(0) : null;
		}
		if (imageUrl != null && imageUrl.length() > 0) {
			glide
					.load(imageUrl)
					.apply(new RequestOptions()
							.error(R.drawable.circle_battery)
							.circleCrop()
							.diskCacheStrategy(DiskCacheStrategy.ALL))
					.transition(withCrossFade())
					.into(imageAd);
		} else {
			imageAd.setImageResource(R.drawable.circle_battery);
		}

		// name
		String name = ad.name != null ? ad.name.trim() : "";
		if (name.length() == 0) name = "---";
		textTitle.setText(name);

		// category
		Category category = ad.category != null ? AdsApp.component().categories().getCategory(ad.category) : null;
		String categoryName = category != null ? category.name : "---";

		SpannableStringBuilder ssbCategory = new SpannableStringBuilder();
		ssbCategory.append(textCategory.getContext().getString(R.string.category_2)).append(' ');
		int st = ssbCategory.length();
		ssbCategory.append(categoryName);
		ssbCategory.setSpan(new ForegroundColorSpan(Color.WHITE), st, ssbCategory.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
		textCategory.setText(ssbCategory, TextView.BufferType.SPANNABLE);

		// subcategory
		Category subCategory = ad.subCategory != null ? AdsApp.component().categories().getCategory(ad.subCategory) : null;
		String subCategoryName = subCategory != null ? subCategory.name : "---";
		SpannableStringBuilder ssbSubCategory = new SpannableStringBuilder();
		ssbSubCategory.append(textCategory.getContext().getString(R.string.subcategory_2)).append(' ');
		st = ssbSubCategory.length();
		ssbSubCategory.append(subCategoryName);
		ssbSubCategory.setSpan(new ForegroundColorSpan(Color.WHITE), st, ssbSubCategory.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
		textSubcategory.setText(ssbSubCategory, TextView.BufferType.SPANNABLE);
	}

}