package com.adsnearme.app.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.adsnearme.app.R;
import com.adsnearme.app.arch.viewmodel.ViewBeaconViewModel;
import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.ui.fragment.MyBeaconAdsFragment;
import com.adsnearme.app.ui.fragment.MyBeaconDataFragment;

public class ViewBeaconActivity extends AppCompatActivity {

	public static final String EXT_BEACON_ID = ViewBeaconActivity.class.getName() + ".EXT_BEACON_ID";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_beacon);

		String beaconId = getIntent().getStringExtra(EXT_BEACON_ID);
		if (beaconId == null) {
			finish();
			return;
		}

		ViewBeaconViewModel viewBeaconViewModel = ViewModelProviders.of(this).get(ViewBeaconViewModel.class);
		viewBeaconViewModel.getBeacon(beaconId).observe(this, this::onBeaconLoaded);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		ActionBar actionBar = getSupportActionBar();
		assert actionBar != null;
		actionBar.setDisplayHomeAsUpEnabled(true);

		ViewBeaconPagerAdapter pagerAdapter = new ViewBeaconPagerAdapter(getSupportFragmentManager(), this, beaconId);
		ViewPager viewPager = findViewById(R.id.view_pager);
		viewPager.setAdapter(pagerAdapter);
	}

	@Override
	public boolean onSupportNavigateUp() {
		super.onBackPressed();
		return true;
	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.slide_in_activity_finish, R.anim.slide_out_activity_finish);
	}

	private void onBeaconLoaded(Beacon beacon) {
		String name = beacon.name != null ? beacon.name.trim() : "";
		if (name.length() == 0) name = "(" + beacon.uuid + ")";
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setSubtitle(name);
		}
	}

	// =======

	private static class ViewBeaconPagerAdapter extends FragmentStatePagerAdapter {

		private final String beaconId;
		private final String titleBeaconData;
		private final String titleBeaconAds;

		ViewBeaconPagerAdapter(FragmentManager fm, Context context, String beaconId) {
			super(fm);
			this.beaconId = beaconId;
			titleBeaconData = context.getString(R.string.beacon_data);
			titleBeaconAds = context.getString(R.string.beacon_ads);
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
				case 0:
					return new MyBeaconDataFragment();
				case 1:
					return MyBeaconAdsFragment.newInstance(beaconId);
			}
			return null;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case 0:
					return titleBeaconData;
				case 1:
					return titleBeaconAds;
			}
			return null;
		}
	}

}