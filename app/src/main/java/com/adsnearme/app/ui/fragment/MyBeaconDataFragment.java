package com.adsnearme.app.ui.fragment;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.adsnearme.app.R;
import com.adsnearme.app.arch.viewmodel.ViewBeaconViewModel;
import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.ui.activity.EditGeofenceRadiusActivity;
import com.adsnearme.app.ui.view.WaveView;
import com.adsnearme.app.ui.view.WaveViewHelper;
import com.adsnearme.app.utils.LocationHelper;
import com.adsnearme.app.utils.Utils;
import com.google.android.gms.maps.model.LatLng;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyBeaconDataFragment extends Fragment {

	private static final int REQ_CODE_EDIT_GEOFENCE_RADIUS = 1;

	@BindView(R.id.text_beacon_name)
	TextView textBeaconName;
	@BindView(R.id.image_beacon)
	ImageView imageBeacon;
	@BindView(R.id.switch_is_stationary)
	Switch switchIsStationary;
	@BindView(R.id.text_geolocation)
	TextView textGeoLocation;
	@BindView(R.id.text_geofencing)
	TextView textGeofencing;
	@BindView(R.id.wave)
	WaveView waveView;
	@BindView(R.id.edit_beacon_name)
	EditText editBeaconName;
	@BindView(R.id.button_edit_name)
	ImageButton buttonEditName;

	private WaveViewHelper waveViewHelper;

	private ViewBeaconViewModel viewBeaconViewModel;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_my_beacon_data, parent, false);
	}

	@Override
	public void onViewCreated(@NonNull View root, Bundle savedInstanceState) {
		ButterKnife.bind(this, root);

		FragmentActivity activity = getActivity();
		assert activity != null;

		viewBeaconViewModel = ViewModelProviders.of(activity).get(ViewBeaconViewModel.class);
		viewBeaconViewModel.getBeacon()
				.observe(this, this::onBeaconLoaded);
		viewBeaconViewModel.getIsInEditNameMode()
				.observe(this, this::onEditNameModeChanged);

		waveView.setWaterLevelRatio(0.4f);
		waveViewHelper = new WaveViewHelper(waveView);
	}

	@Override
	public void onStart() {
		super.onStart();
		waveViewHelper.start();
	}

	@Override
	public void onStop() {
		waveViewHelper.stop();
		super.onStop();
	}

	private void onBeaconLoaded(Beacon beacon) {
		// name
		String name = beacon.name != null ? beacon.name.trim() : "";
		if (name.length() == 0) name = "---";
		textBeaconName.setText(name);
		editBeaconName.setText(name);
		editBeaconName.setSelection(editBeaconName.length());

		// color
		Integer color = null;
		if (beacon.color != null) {
			try {
				color = Color.parseColor(beacon.color);
			} catch (Exception ignored) {
			}
		}
		if (color != null) {
			Context context = getContext();
			if (context != null) {
				Drawable icon = ContextCompat.getDrawable(context, R.drawable.ic_beacon_bold);
				if (icon != null) {
					icon.mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
					imageBeacon.setImageDrawable(icon);
				} else {
					imageBeacon.setImageResource(R.drawable.ic_beacon_bold);
				}
			}
		} else {
			imageBeacon.setImageResource(R.drawable.ic_beacon_bold);
		}

		// type
		boolean isStationary = beacon.type == null || beacon.type == Beacon.Type.STATIONARY;
		switchIsStationary.setEnabled(true);
		switchIsStationary.setOnCheckedChangeListener(null);
		switchIsStationary.setChecked(isStationary);
		switchIsStationary.setOnCheckedChangeListener(typeChangeListener);

		// geolocation
		if (beacon.latitude != null && beacon.longitude != null && beacon.latitude != 0 && beacon.longitude != 0) {
			String strLocation = LocationHelper.toDegreesString(beacon.latitude, beacon.longitude);
			textGeoLocation.setText(strLocation);
		} else {
			textGeoLocation.setText("---");
		}

		// geofencing
		if (beacon.geofenceRadius != null) {
			textGeofencing.setText(LocationHelper.distanceToString(beacon.geofenceRadius));
		} else {
			textGeofencing.setText("---");
		}

		onEditNameModeChanged(false);
	}

	private CompoundButton.OnCheckedChangeListener typeChangeListener = new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			Beacon beacon = viewBeaconViewModel.getBeaconValue();
			if (beacon != null) {
				Beacon.Type currentType = beacon.type != null ? beacon.type : Beacon.Type.STATIONARY;
				Beacon.Type newType = isChecked ? Beacon.Type.STATIONARY : Beacon.Type.MOVING;
				if (currentType != newType) {
					switchIsStationary.setEnabled(false);
					viewBeaconViewModel.setBeaconType(beacon, newType);
				}
				return;
			}
			switchIsStationary.setOnCheckedChangeListener(null);
			switchIsStationary.setChecked(!isChecked);
			switchIsStationary.setOnCheckedChangeListener(this);
		}
	};

	private void onEditNameModeChanged(boolean isInEditNameMode) {
		if (isInEditNameMode) {
			textBeaconName.setVisibility(View.INVISIBLE);

			editBeaconName.setVisibility(View.VISIBLE);
			editBeaconName.setText(textBeaconName.getText());
			editBeaconName.setSelection(editBeaconName.length());

			buttonEditName.setImageResource(R.drawable.ic_done);

			Utils.setKeyboardVisible(getContext(), editBeaconName, true);
		} else {
			textBeaconName.setVisibility(View.VISIBLE);

			editBeaconName.setVisibility(View.INVISIBLE);
			editBeaconName.setText(textBeaconName.getText());
			editBeaconName.setSelection(editBeaconName.length());

			buttonEditName.setImageResource(R.drawable.ic_edit);

			Utils.setKeyboardVisible(getContext(), editBeaconName, false);
		}
	}

	@OnClick(R.id.button_edit_name)
	public void onEditNameButtonClicked() {
		if (viewBeaconViewModel.isInEditNameMode()) {
			String newName = editBeaconName.getText().toString().trim();
			if (newName.length() == 0) {
				Toast.makeText(getContext(), R.string.please_enter_beacon_name, Toast.LENGTH_SHORT).show();
				editBeaconName.requestFocus();
				return;
			}
			Beacon beacon = viewBeaconViewModel.getBeaconValue();
			if (beacon != null) {
				String oldName = beacon.name != null ? beacon.name.trim() : "";
				if (!newName.equals(oldName)) {
					// изменилось имя, сохраняем на сервере
					textBeaconName.setText(newName);
					viewBeaconViewModel.setBeaconName(beacon, newName);
				}
			}
			// выключаем режим редактирования
			viewBeaconViewModel.setIsInEditNameMode(false);

			Utils.setKeyboardVisible(getContext(), editBeaconName, false);
		} else {
			// включаем режим редактирования
			viewBeaconViewModel.setIsInEditNameMode(true);
		}
	}

	@OnClick(R.id.button_edit_radius)
	public void onEditRadiusButtonClicked() {
		Beacon beacon = viewBeaconViewModel.getBeaconValue();
		if (beacon != null && beacon.latitude != null && beacon.longitude != null && beacon.latitude != 0 && beacon.longitude != 0) {
			int radius = beacon.geofenceRadius != null ? beacon.geofenceRadius.intValue() : 0;
			Intent intent = new Intent(getContext(), EditGeofenceRadiusActivity.class);
			intent.putExtra(EditGeofenceRadiusActivity.EXT_LATLNG, new LatLng(beacon.latitude, beacon.longitude));
			intent.putExtra(EditGeofenceRadiusActivity.EXT_RADIUS, radius);
			intent.putExtra(EditGeofenceRadiusActivity.EXT_NAME, beacon.name);
			startActivityForResult(intent, REQ_CODE_EDIT_GEOFENCE_RADIUS);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case REQ_CODE_EDIT_GEOFENCE_RADIUS:
				if (resultCode == Activity.RESULT_OK && data != null) {
					// изменился радиус, сохраняем на сервере
					int radius = data.getIntExtra(EditGeofenceRadiusActivity.RESULT_RADIUS, 0);
					if (radius != 0) {
						Beacon beacon = viewBeaconViewModel.getBeaconValue();
						if (beacon != null) {
							textGeofencing.setText(LocationHelper.distanceToString(radius));
							viewBeaconViewModel.setBeaconRadius(beacon, radius);
						}
					}
				}
				break;
			default:
				super.onActivityResult(requestCode, resultCode, data);
		}
	}

}