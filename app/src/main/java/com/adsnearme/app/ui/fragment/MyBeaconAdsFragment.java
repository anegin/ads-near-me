package com.adsnearme.app.ui.fragment;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.adsnearme.app.R;
import com.adsnearme.app.arch.viewmodel.BeaconAdsViewModel;
import com.adsnearme.app.arch.viewmodel.ViewBeaconViewModel;
import com.adsnearme.app.model.NoError;
import com.adsnearme.app.model.api.Ad;
import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.model.api.response.ApiException;
import com.adsnearme.app.ui.adapter.BeaconAdsAdapter;
import com.adsnearme.app.ui.adapter.viewholder.BeaconAdViewHolder;
import com.adsnearme.app.utils.network.NoNetworkException;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyBeaconAdsFragment extends Fragment implements BeaconAdViewHolder.Listener, SwipeRefreshLayout.OnRefreshListener {

	private static final String ARG_BEACON_ID = MyBeaconAdsFragment.class.getName() + ".ARG_BEACON_ID";

	private static final String STATE_IS_LOADED_FROM_SERVER = MyBeaconAdsFragment.class.getName() + ".STATE_IS_LOADED_FROM_SERVER";

	@BindView(R.id.swipe_refresh_layout)
	SwipeRefreshLayout swipeRefreshLayout;
	@BindView(R.id.recyclerview_ads)
	RecyclerView recyclerViewAds;
	@BindView(R.id.text_no_ads)
	TextView textNoAds;

	@BindView(R.id.layout_error)
	View layoutError;
	@BindView(R.id.text_error)
	TextView textError;

	private BeaconAdsAdapter adapter;

	private ViewBeaconViewModel viewBeaconViewModel;
	private BeaconAdsViewModel beaconAdsViewModel;

	private String beaconId;

	public static Fragment newInstance(String beaconId) {
		Bundle args = new Bundle();
		args.putString(ARG_BEACON_ID, beaconId);
		Fragment fragment = new MyBeaconAdsFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if (args != null) {
			beaconId = args.getString(ARG_BEACON_ID);
		}
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_my_beacon_ads, parent, false);
	}

	@Override
	public void onViewCreated(@NonNull View root, Bundle savedInstanceState) {
		ButterKnife.bind(this, root);

		FragmentActivity activity = getActivity();
		assert activity != null;

		adapter = new BeaconAdsAdapter(Glide.with(this), this);

		recyclerViewAds.setLayoutManager(new LinearLayoutManager(activity));
		recyclerViewAds.setAdapter(adapter);
		recyclerViewAds.addItemDecoration(new DividerItemDecoration(activity, LinearLayoutManager.VERTICAL));

		swipeRefreshLayout.setOnRefreshListener(this);

		viewBeaconViewModel = ViewModelProviders.of(activity).get(ViewBeaconViewModel.class);

		beaconAdsViewModel = ViewModelProviders.of(this).get(BeaconAdsViewModel.class);
		beaconAdsViewModel.getBeaconAds(beaconId).observe(this, this::onBeaconAdsListLoaded);
		beaconAdsViewModel.getError().observe(this, this::onError);

		if (savedInstanceState != null) {
			beaconAdsViewModel.setIsLoadedFromServer(savedInstanceState.getBoolean(STATE_IS_LOADED_FROM_SERVER));
		}

		if (!beaconAdsViewModel.isLoadedFromServer()) {
			loadFromServer();
		}
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(STATE_IS_LOADED_FROM_SERVER, beaconAdsViewModel.isLoadedFromServer());
	}

	@Override
	public void onRefresh() {
		loadFromServer();
	}

	@OnClick(R.id.button_error_retry)
	public void loadFromServer() {
		if (!swipeRefreshLayout.isRefreshing()) {
			swipeRefreshLayout.setRefreshing(true);
		}
		layoutError.setVisibility(View.GONE);

		LiveData<Beacon> beaconLiveData = viewBeaconViewModel.getBeacon();
		beaconLiveData.observeForever(new Observer<Beacon>() {
			@Override
			public void onChanged(@Nullable Beacon beacon) {
				if (beacon != null) {
					beaconAdsViewModel.loadFromServer(beacon);
					beaconLiveData.removeObserver(this);
				}
			}
		});
	}

	private void onBeaconAdsListLoaded(List<Ad> ads) {
		if (swipeRefreshLayout.isRefreshing()) {
			swipeRefreshLayout.setRefreshing(false);
		}
		adapter.setAds(ads);
		textNoAds.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
		layoutError.setVisibility(View.GONE);
	}

	private void onError(Throwable err) {
		if (swipeRefreshLayout.isRefreshing()) {
			swipeRefreshLayout.setRefreshing(false);
		}
		if (err instanceof NoError) {
			layoutError.setVisibility(View.GONE);
			return;
		}
		String errorMessage;
		if (err instanceof NoNetworkException) {
			errorMessage = getString(R.string.no_network_connection);
		} else if (err instanceof ApiException) {
			errorMessage = ((ApiException) err).text;
		} else {
			errorMessage = getString(R.string.error_requesting_beacons_list);
		}
		if (adapter.getItemCount() > 0) {
			layoutError.setVisibility(View.GONE);
			Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
		} else {
			layoutError.setVisibility(View.VISIBLE);
			textError.setText(errorMessage);
		}
	}

	@Override
	public void onAdClicked(Ad ad) {
		// todo view Ad
		Toast.makeText(getContext(), "Coming soon...", Toast.LENGTH_SHORT).show();
	}

}