package com.adsnearme.app.ui.adapter.viewholder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adsnearme.app.R;
import com.adsnearme.app.model.api.Beacon;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BeaconViewHolder extends InflaterViewHolder {

	@BindView(R.id.text_beacon_name)
	TextView textBeaconName;
	@BindView(R.id.text_marker)
	TextView textMarker;

	private Beacon beacon;

	public interface Listener {
		void onBeaconClicked(Beacon beacon);
	}

	public BeaconViewHolder(ViewGroup parent, Listener listener) {
		super(R.layout.listitem_beacon, parent);
		ButterKnife.bind(this, itemView);

		itemView.setOnClickListener(v -> {
			if (listener != null && beacon != null) {
				listener.onBeaconClicked(beacon);
			}
		});
	}

	public void bind(Beacon beacon) {
		this.beacon = beacon;

		// ads count
		int adsCount = beacon.ads != null ? beacon.ads.size() : 0;
		if (adsCount > 0) {
			textMarker.setVisibility(View.VISIBLE);
			textMarker.setText(String.valueOf(adsCount));
		} else {
			textMarker.setVisibility(View.INVISIBLE);
		}

		// name
		String name = beacon.name != null ? beacon.name.trim() : "";
		if (name.length() == 0) {
			String uuid = beacon.uuid != null ? beacon.uuid.trim() : "";
			if (uuid.length() > 0) {
				name = "(" + uuid + ")";
			} else {
				name = "---";
			}
		}
		textBeaconName.setText(name);
	}

}