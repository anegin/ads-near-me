package com.adsnearme.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.R;
import com.adsnearme.app.api.Api;
import com.adsnearme.app.model.AuthData;
import com.adsnearme.app.auth.AuthType;
import com.adsnearme.app.model.api.request.GetUserInfoRequest;
import com.adsnearme.app.model.api.request.RegUserRequest;
import com.adsnearme.app.model.api.response.ApiException;
import com.adsnearme.app.model.api.response.RegUserResponse;
import com.adsnearme.app.utils.ButterKnifeUtils;
import com.adsnearme.app.utils.Validator;
import com.adsnearme.app.utils.network.NoNetworkException;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class SignUpActivity extends BaseLoginActivity {

	@BindView(R.id.edit_name)
	EditText editName;
	@BindView(R.id.edit_surname)
	EditText editSurname;
	@BindView(R.id.edit_email)
	EditText editEmail;
	@BindView(R.id.edit_password)
	EditText editPassword;
	@BindView(R.id.edit_password_confirm)
	EditText editPasswordConfirm;
	@BindView(R.id.button_sign_up)
	Button buttonSignUp;
	@BindView(R.id.button_facebook)
	View buttonFacebook;
	@BindView(R.id.button_sign_in)
	View buttonSignIn;

	@BindViews({R.id.edit_name, R.id.edit_surname, R.id.edit_email, R.id.edit_password, R.id.edit_password_confirm,
			R.id.button_sign_in, R.id.button_sign_up, R.id.button_facebook})
	List<View> clickableViews;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		ButterKnife.bind(this);
		checkSignUpButton();
	}

	@Override
	public void showProgress() {
		ButterKnife.apply(clickableViews, ButterKnifeUtils.ENABLED, false);
	}

	@Override
	public void hideProgress() {
		ButterKnife.apply(clickableViews, ButterKnifeUtils.ENABLED, true);
	}

	@OnClick(R.id.button_sign_in)
	public void showSignInActivity() {
		startActivity(new Intent(this, SignInActivity.class));
		finish();
	}

	@OnTextChanged({R.id.edit_name, R.id.edit_surname, R.id.edit_email, R.id.edit_password, R.id.edit_password_confirm})
	public void checkSignUpButton() {
		buttonSignUp.setEnabled(editName.length() > 0
				&& editSurname.length() > 0
				&& editEmail.length() > 0
				&& editPassword.length() > 0
				&& editPasswordConfirm.length() > 0);
	}

	@OnClick(R.id.button_sign_up)
	public void signUp() {
		String name = editName.getText().toString().trim();
		String surname = editSurname.getText().toString().trim();
		String email = editEmail.getText().toString().trim();
		String password = editPassword.getText().toString().trim();
		String passwordConfirm = editPasswordConfirm.getText().toString().trim();

		if (TextUtils.isEmpty(name)) {
			Toast.makeText(this, R.string.please_enter_user_name, Toast.LENGTH_SHORT).show();
			editName.requestFocus();
			editName.setSelection(editName.length());
			return;
		}
		if (TextUtils.isEmpty(surname)) {
			Toast.makeText(this, R.string.please_enter_surname, Toast.LENGTH_SHORT).show();
			editSurname.requestFocus();
			editSurname.setSelection(editName.length());
			return;
		}
		if (TextUtils.isEmpty(email)) {
			Toast.makeText(this, R.string.please_enter_email, Toast.LENGTH_SHORT).show();
			editEmail.requestFocus();
			editEmail.setSelection(editEmail.length());
			return;
		}
		if (Validator.isEmailInvalid(this, email)) {
			Toast.makeText(this, R.string.please_check_entered_email, Toast.LENGTH_SHORT).show();
			editEmail.requestFocus();
			editEmail.setSelection(editEmail.length());
			return;
		}
		if (TextUtils.isEmpty(password)) {
			Toast.makeText(this, R.string.please_enter_password, Toast.LENGTH_SHORT).show();
			editPassword.requestFocus();
			editPassword.setSelection(editPassword.length());
			return;
		}
		if (TextUtils.isEmpty(passwordConfirm)) {
			Toast.makeText(this, R.string.please_enter_password_confirmation, Toast.LENGTH_SHORT).show();
			editPasswordConfirm.requestFocus();
			editPasswordConfirm.setSelection(editPassword.length());
			return;
		}
		if (!password.equals(passwordConfirm)) {
			Toast.makeText(this, R.string.passwords_do_not_match, Toast.LENGTH_SHORT).show();
			editPasswordConfirm.requestFocus();
			editPasswordConfirm.setSelection(editPassword.length());
			return;
		}
		showProgress();

		AdsApp.component().auth().setAuthData(null);

		Api.registerUser(new RegUserRequest(email, password, name, surname))
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this::onUserRegisterSuccess, this::onUserRegisterError);
	}

	private void onUserRegisterSuccess(RegUserResponse regUserResponse) {
		AuthData authData = new AuthData(AuthType.EMAIL);
		authData.sessionId = regUserResponse.sessionId;
		authData.sessionsName = regUserResponse.sessionName;
		AdsApp.component().auth().setAuthData(authData);

		Api.getUserInfo(new GetUserInfoRequest())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(response -> onUserLoginSuccess(response, authData), this::onUserRegisterError);
	}

	private void onUserRegisterError(Throwable t) {
		AdsApp.component().auth().setAuthData(null);
		hideProgress();
		if (t instanceof ApiException) {
			ApiException apiException = (ApiException) t;
			if (ApiException.USER_ALREADY_EXISTS.equals(apiException.code)) {
				Toast.makeText(this, R.string.error_login_already_in_use, Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this, R.string.registration_failed, Toast.LENGTH_SHORT).show();
			}
		} else if (t instanceof NoNetworkException) {
			Toast.makeText(this, R.string.no_network_connection, Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, R.string.communication_error_with_server, Toast.LENGTH_SHORT).show();
		}
	}

	@OnClick(R.id.button_facebook)
	public void facebookLogin() {
		showProgress();
		loginWithFacebook();
	}

}