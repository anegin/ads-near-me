package com.adsnearme.app.ui.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.R;
import com.adsnearme.app.arch.viewmodel.LocationViewModel;
import com.adsnearme.app.model.AuthData;
import com.adsnearme.app.utils.DateUtils;
import com.adsnearme.app.utils.LocationHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;

public class MyProfileDataFragment extends Fragment {

	@BindView(R.id.text_username)
	TextView textUserName;
	@BindView(R.id.text_email)
	TextView textEmail;
	@BindView(R.id.text_geolocation)
	TextView textGeolocation;
	@BindView(R.id.text_left_ads)
	TextView textLeftAds;
	@BindView(R.id.text_end_of_subscription)
	TextView textEndOfSubscription;
	@BindView(R.id.button_renew_subscription)
	Button buttonRenewMySubscription;

	private Disposable rxDisposable;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_my_profile_data, parent, false);
	}

	@Override
	public void onViewCreated(@NonNull View root, Bundle savedInstanceState) {
		ButterKnife.bind(this, root);

		rxDisposable = AdsApp.component().auth().observer()
				.subscribe(this::updateUi);

		FragmentActivity activity = getActivity();
		assert activity != null;
		ViewModelProviders.of(activity).get(LocationViewModel.class)
				.getLocation()
				.observe(this, this::updateLocation);

		if (textGeolocation.length() == 0) {
			updateLocation(null);
		}
	}

	@Override
	public void onDestroyView() {
		if (rxDisposable != null) {
			if (!rxDisposable.isDisposed()) {
				rxDisposable.dispose();
			}
			rxDisposable = null;
		}
		super.onDestroyView();
	}

	@OnClick(R.id.button_renew_subscription)
	public void renewMySubscription() {
		// todo renew my subscription
	}

	private void updateUi(AuthData authData) {
		if (authData.user == null) return;

		// username
		String firstName = authData.user.name != null ? authData.user.name.trim() : "";
		String lastName = authData.user.surname != null ? authData.user.surname.trim() : "";
		String userName = firstName;
		if (lastName.length() > 0) {
			if (userName.length() > 0) userName += " ";
			userName += lastName;
		}
		textUserName.setText(userName);

		// email
		String email = authData.user.email != null ? authData.user.email.trim() : "";
		if (email.length() > 0) {
			textEmail.setText(email);
		} else {
			textEmail.setText("---");
		}

		// left ads
		textLeftAds.setText(String.valueOf(authData.user.adCount));

		// end of subscription
		if (authData.user.paidTo != null) {
			String paidToDate = DateUtils.formatDateShort(authData.user.paidTo);
			textEndOfSubscription.setText(paidToDate);
			buttonRenewMySubscription.setEnabled(true);
		} else {
			textEndOfSubscription.setText("---");
			buttonRenewMySubscription.setEnabled(false);
		}
	}

	private void updateLocation(Location location) {
		if (LocationHelper.hasPermission() && location != null) {
			String strLocation = LocationHelper.toDegreesString(location.getLatitude(), location.getLongitude());
			textGeolocation.setText(strLocation);
		} else {
			textGeolocation.setText(R.string.not_available);
		}
	}

}