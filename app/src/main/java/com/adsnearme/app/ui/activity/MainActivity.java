package com.adsnearme.app.ui.activity;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.R;
import com.adsnearme.app.api.Api;
import com.adsnearme.app.arch.viewmodel.LocationViewModel;
import com.adsnearme.app.geofence.UpdateGeofencesJob;
import com.adsnearme.app.ui.fragment.AdsListFragment;
import com.adsnearme.app.ui.fragment.MyProfileFragment;
import com.adsnearme.app.utils.LocationHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

	private static final String TAG = MainActivity.class.getSimpleName();

	private static final int REQ_PERMISSION_LOCATION = 1;

	@BindView(R.id.drawer_layout)
	DrawerLayout drawer;

	private LocationViewModel locationViewModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		locationViewModel = ViewModelProviders.of(this).get(LocationViewModel.class);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();

		NavigationView navigationView = findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);

		if (savedInstanceState == null) {
			showAdsListFragment();
		}

		if (!LocationHelper.hasPermission()) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQ_PERMISSION_LOCATION);
		}

		requestAppInfo();

		UpdateGeofencesJob.startNow();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		switch (requestCode) {
			case REQ_PERMISSION_LOCATION:
				if (LocationHelper.hasPermission()) {
					locationViewModel.startLocationUpdates();
				}
				break;
		}
	}

	@Override
	public void onBackPressed() {
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
		switch (item.getItemId()) {
			case R.id.nav_my_profile:
				showMyProfileFragment();
				break;
			case R.id.nav_favourites:
				showEmptyFragment(R.string.nav_favourites);
				break;
			case R.id.nav_tutorial:
				showEmptyFragment(R.string.nav_tutorial);
				break;
			case R.id.nav_ads:
				showAdsListFragment();
				break;
			case R.id.nav_create_ad:
				showEmptyFragment(R.string.nav_create_ad);
				break;
			case R.id.nav_logout:
				confirmLogout();
				break;
		}
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	private void confirmLogout() {
		new AlertDialog.Builder(this)
				.setTitle(R.string.confirmation)
				.setMessage(R.string.do_you_want_to_logout)
				.setCancelable(true)
				.setPositiveButton(R.string.yes, (d, w) -> logout())
				.setNegativeButton(R.string.cancel, null)
				.show();
	}

	private void logout() {
		AdsApp.component().auth().setAuthData(null);
		startActivity(new Intent(this, SignInActivity.class));
		finishAffinity();
	}

	private void showMyProfileFragment() {
		Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_content);
		if (!(fragment instanceof MyProfileFragment)) {
			fragment = new MyProfileFragment();
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.layout_content, fragment)
					.commit();
		}
		setTitle(R.string.nav_my_profile);
	}

	private void showAdsListFragment() {
		Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_content);
		if (!(fragment instanceof AdsListFragment)) {
			fragment = new AdsListFragment();
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.layout_content, fragment)
					.commit();
		}
		setTitle(R.string.nav_ads);
	}

	private void showEmptyFragment(int titleResId) {
		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.layout_content, new Fragment())
				.commit();
		setTitle(titleResId);
	}

	private void requestAppInfo() {
		Api.getAppInfo()
				.observeOn(Schedulers.newThread())
				.subscribe(response -> AdsApp.component().categories().updateCategories(response.answer.categories),
						t -> Log.w(TAG, "Error loading app info", t));
	}

}