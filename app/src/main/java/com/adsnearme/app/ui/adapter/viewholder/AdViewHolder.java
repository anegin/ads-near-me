package com.adsnearme.app.ui.adapter.viewholder;

import android.graphics.Color;
import android.location.Location;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.Constants;
import com.adsnearme.app.R;
import com.adsnearme.app.model.Category;
import com.adsnearme.app.model.api.Ad;
import com.adsnearme.app.utils.DateUtils;
import com.adsnearme.app.utils.LocationHelper;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import org.joda.time.DateTime;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class AdViewHolder extends InflaterViewHolder {

	@BindView(R.id.image_ad)
	ImageView imageAd;
	@BindView(R.id.text_title_and_distance)
	TextView textTitleAndDistance;
	@BindView(R.id.text_description)
	TextView textDescription;
	@BindView(R.id.text_date_category)
	TextView textDateCategory;
	@BindView(R.id.button_toggle_favorite)
	ImageButton buttonToggleFavorite;
	@BindView(R.id.button_share)
	ImageButton buttonShare;

	private Ad ad;

	public interface Listener {
		void onAdClicked(Ad ad);

		void onToggleAdFavorite(Ad ad);

		void onShareAdClicked(Ad ad);
	}

	public AdViewHolder(ViewGroup parent, Listener listener) {
		super(R.layout.listitem_ad, parent);
		ButterKnife.bind(this, itemView);

		itemView.setOnClickListener(v -> {
			if (listener != null && ad != null) listener.onAdClicked(ad);
		});
		buttonToggleFavorite.setOnClickListener(v -> {
			if (listener != null && ad != null) listener.onToggleAdFavorite(ad);
		});
		buttonShare.setOnClickListener(v -> {
			if (listener != null && ad != null) listener.onShareAdClicked(ad);
		});
	}

	public void bind(Ad ad, RequestManager glide, Location myLocation, boolean distanceInMeters) {
		this.ad = ad;

		// image
		String imageUrl = ad.getImageUrl();
		if (imageUrl != null && imageUrl.length() > 0) {
			glide
					.load(imageUrl)
					.apply(new RequestOptions()
							.error(R.drawable.circle_battery)
							.placeholder(R.drawable.circle_battery)
							.circleCrop()
							.diskCacheStrategy(DiskCacheStrategy.ALL))
					.transition(withCrossFade())
					.into(imageAd);
		} else {
			imageAd.setImageResource(R.drawable.circle_battery);
		}

		// name and distance
		String name = ad.name != null ? ad.name.trim() : "";
		if (name.length() == 0) name = "---";
		Double distanceMeters = null;
		if (myLocation != null && ad.latitude != null && ad.longitude != null && ad.latitude != 0 && ad.longitude != 0) {
			distanceMeters = SphericalUtil.computeDistanceBetween(new LatLng(ad.latitude, ad.longitude),
					new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
		} else if (ad.distance != null) {
			distanceMeters = LocationHelper.milesToMeters(ad.distance);
		}
		String distanceString = null;
		if (distanceMeters != null) {
			if (distanceInMeters) {
				distanceString = LocationHelper.distanceToString(distanceMeters);
			} else {
				double distanceMinutes = distanceMeters / (1000 * Constants.WALKING_SPEED_KMH / 60);
				distanceString = DateUtils.formatPeriod((long) distanceMinutes);
			}
		}
		if (distanceString != null) {
			SpannableStringBuilder ssbName = new SpannableStringBuilder();
			ssbName.append(name).append(' ');
			int st = ssbName.length();
			ssbName.append(distanceString);
			int accentColor = ContextCompat.getColor(textDateCategory.getContext(), R.color.color_accent);
			ssbName.setSpan(new ForegroundColorSpan(accentColor), st, ssbName.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
			textTitleAndDistance.setText(ssbName, TextView.BufferType.SPANNABLE);
		} else {
			textTitleAndDistance.setText(name);
		}

		// description
		String description = ad.description != null ? ad.description.trim() : "";
		if (description.length() > 0) {
			textDescription.setText(description);
			textDescription.setVisibility(View.VISIBLE);
		} else {
			textDescription.setVisibility(View.GONE);
		}

		// date, category
		DateTime date = ad.dateUpdated != null ? ad.dateUpdated : ad.dateAdded;
		String dateFormatted = date != null ? DateUtils.formatDateLong(date) : "";
		Category category = ad.category != null ? AdsApp.component().categories().getCategory(ad.category) : null;
		String categoryName = category != null ? category.name : "";
		Category subCategory = ad.subCategory != null ? AdsApp.component().categories().getCategory(ad.subCategory) : null;
		String subCategoryName = subCategory != null ? subCategory.name : "";

		SpannableStringBuilder ssbDateCategory = new SpannableStringBuilder();
		if (dateFormatted.length() > 0) {
			ssbDateCategory.append(dateFormatted).append(' ');
		}
		int st = ssbDateCategory.length() + 2;
		boolean categoryAdded = false;
		if (categoryName.length() > 0) {
			if (ssbDateCategory.length() > 0) {
				ssbDateCategory.append(", ");
			}
			ssbDateCategory.append(categoryName);
			categoryAdded = true;
		}
		if (subCategoryName.length() > 0) {
			if (categoryAdded) {
				ssbDateCategory.append(" - ");
			} else if (ssbDateCategory.length() > 0) {
				ssbDateCategory.append(", ");
			}
			ssbDateCategory.append(subCategoryName);
		}
		if (ssbDateCategory.length() > st) {
			ssbDateCategory.setSpan(new ForegroundColorSpan(Color.WHITE), st, ssbDateCategory.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
		}
		textDateCategory.setText(ssbDateCategory, TextView.BufferType.SPANNABLE);

		// is favorite
		buttonToggleFavorite.setEnabled(true);
		buttonToggleFavorite.setImageResource(ad.inFavorites == 1 ? R.drawable.ic_ad_star_red : R.drawable.ic_ad_star);

		// share
		buttonShare.setEnabled(true);

		itemView.setEnabled(true);
	}

	public void clear() {
		imageAd.setImageResource(R.drawable.circle_battery);
		textTitleAndDistance.setText("");
		textDescription.setText("");
		textDateCategory.setText("");

		buttonToggleFavorite.setImageAlpha(R.drawable.ic_ad_star);
		buttonToggleFavorite.setEnabled(false);

		buttonShare.setEnabled(false);

		itemView.setEnabled(false);
	}

}