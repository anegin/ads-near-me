package com.adsnearme.app.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.adsnearme.app.R;
import com.adsnearme.app.arch.viewmodel.EditGeofenceRadiusViewModel;
import com.adsnearme.app.utils.LocationHelper;
import com.adsnearme.app.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditGeofenceRadiusActivity extends AppCompatActivity implements OnMapReadyCallback {

	public static final String EXT_NAME = EditGeofenceRadiusActivity.class.getName() + ".EXT_NAME";
	public static final String EXT_LATLNG = EditGeofenceRadiusActivity.class.getName() + ".EXT_LATLNG";
	public static final String EXT_RADIUS = EditGeofenceRadiusActivity.class.getName() + ".EXT_RADIUS";

	public static final String RESULT_RADIUS = EditGeofenceRadiusActivity.class.getName() + ".RESULT_RADIUS";

	private static final int MIN_RADIUS = 100;
	private static final int MAX_RADIUS = 5000;
	private static final int RADIUS_STEP = 100;

	@BindView(R.id.toolbar)
	Toolbar toolbar;
	@BindView(R.id.layout_bottom)
	View layoutBottom;
	@BindView(R.id.text_map_not_available)
	TextView textMapNotAvailable;
	@BindView(R.id.seekbar_radius)
	SeekBar seekBarRadius;
	@BindView(R.id.text_radius)
	TextView textRadius;
	@BindView(R.id.button_apply)
	Button buttonApply;

	private GoogleMap googleMap;
	private Circle circle;

	private LatLng location;
	private int originalRadius;
	private String name;

	private EditGeofenceRadiusViewModel viewModel;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_geofence_radius);
		ButterKnife.bind(this);

		setResult(RESULT_CANCELED);

		location = getIntent().getParcelableExtra(EXT_LATLNG);
		originalRadius = getIntent().getIntExtra(EXT_RADIUS, 0);
		name = getIntent().getStringExtra(EXT_NAME);
		if (location == null || originalRadius == 0) {
			finish();
			return;
		}

		if (originalRadius < MIN_RADIUS) {
			originalRadius = MIN_RADIUS;
		} else if (originalRadius > MAX_RADIUS) {
			originalRadius = MAX_RADIUS;
		}

		viewModel = ViewModelProviders.of(this).get(EditGeofenceRadiusViewModel.class);
		viewModel.getRadius().observe(this, this::onRadiusChanged);
		viewModel.setRadius(originalRadius);

		setSupportActionBar(toolbar);

		ActionBar actionBar = getSupportActionBar();
		assert actionBar != null;
		actionBar.setDisplayHomeAsUpEnabled(true);
		if (name != null) {
			actionBar.setSubtitle(name);
		}

		int max = (MAX_RADIUS - MIN_RADIUS) / RADIUS_STEP;
		int progress = (originalRadius - MIN_RADIUS) / RADIUS_STEP;
		seekBarRadius.setMax(max);
		seekBarRadius.setProgress(progress);
		seekBarRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					int radius = MIN_RADIUS + progress * RADIUS_STEP;
					viewModel.setRadius(radius);
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		int result = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
		if (result == ConnectionResult.SUCCESS) {
			Fragment mapFragment = getSupportFragmentManager().findFragmentByTag("map");
			if (mapFragment == null || !mapFragment.isAdded() || mapFragment.isDetached()) {
				GoogleMapOptions mapOptions = new GoogleMapOptions();
				mapOptions.mapType(GoogleMap.MAP_TYPE_NORMAL);
				mapOptions.liteMode(false);
				mapOptions.mapToolbarEnabled(true);
				mapOptions.compassEnabled(true);
				mapOptions.rotateGesturesEnabled(true);
				mapOptions.scrollGesturesEnabled(true);
				mapOptions.tiltGesturesEnabled(true);
				mapOptions.zoomGesturesEnabled(true);
				mapOptions.zoomControlsEnabled(true);
				mapFragment = SupportMapFragment.newInstance(mapOptions);
				((SupportMapFragment) mapFragment).getMapAsync(this);
				getSupportFragmentManager()
						.beginTransaction()
						.add(R.id.layout_map_fragment, mapFragment, "map")
						.commit();
			}
			textMapNotAvailable.setVisibility(View.GONE);
		} else {
			textMapNotAvailable.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		googleMap.setPadding(0, toolbar.getHeight(), 0, layoutBottom.getHeight());
		googleMap.setBuildingsEnabled(true);

		UiSettings uiSettings = googleMap.getUiSettings();
		uiSettings.setAllGesturesEnabled(true);
		uiSettings.setCompassEnabled(true);
		uiSettings.setMapToolbarEnabled(true);
		uiSettings.setZoomControlsEnabled(true);

		if (LocationHelper.hasPermission()) {
			try {
				googleMap.setMyLocationEnabled(true);
				uiSettings.setMyLocationButtonEnabled(true);
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}

		googleMap.setOnMyLocationButtonClickListener(() -> {
			animateCameraToLocationAndRadius();
			return true;
		});

		googleMap.setOnMapLoadedCallback(() -> {
			googleMap.setOnMapLoadedCallback(null);
			this.googleMap = googleMap;

			// beacon marker
			googleMap.addMarker(new MarkerOptions()
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker))
					.anchor(0.5f, 55f / 66f)
					.title(name)
					.position(location));

			if (circle == null) {
				onRadiusChanged(originalRadius);
			}
		});
	}

	private void onRadiusChanged(int radius) {
		// circle
		if (googleMap != null) {
			if (circle != null) circle.remove();
			circle = googleMap.addCircle(new CircleOptions()
					.center(location)
					.radius(radius)
					.strokeColor(Color.LTGRAY));
		}

		// text
		SpannableStringBuilder ssb = new SpannableStringBuilder();
		ssb.append(LocationHelper.distanceToString(radius));
		int end = ssb.length();
		ssb.append(' ').append(getString(R.string.around_beacon));
		int accentColor = ContextCompat.getColor(this, R.color.color_accent);
		ssb.setSpan(new ForegroundColorSpan(accentColor), 0, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
		ssb.setSpan(new StyleSpan(Typeface.BOLD), 0, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
		textRadius.setText(ssb, TextView.BufferType.SPANNABLE);

		// button
		buttonApply.setEnabled(originalRadius != radius);

		animateCameraToLocationAndRadius();
	}

	private void animateCameraToLocationAndRadius() {
		if (googleMap == null) return;

		Integer radius = viewModel.getRadius().getValue();
		if (radius == null) radius = originalRadius;

		double headingRadius = Math.sqrt(2) * radius;
		LatLng norhtEast = SphericalUtil.computeOffset(location, headingRadius, 45);
		LatLng southWest = SphericalUtil.computeOffset(location, headingRadius, 225);
		LatLngBounds bounds = new LatLngBounds(southWest, norhtEast);

		float padding = Utils.dpToPx(this, 32);
		googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, (int) padding));
	}

	@OnClick(R.id.button_apply)
	public void onApplyButtonClicked() {
		Integer radius = viewModel.getRadius().getValue();
		if (radius != null && originalRadius != radius) {
			Intent resultIntent = new Intent();
			resultIntent.putExtra(RESULT_RADIUS, radius);
			setResult(RESULT_OK, resultIntent);
		}
		finish();
	}

}