package com.adsnearme.app.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.ui.adapter.viewholder.BeaconViewHolder;

import java.util.ArrayList;
import java.util.List;

public class MyBeaconsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private final List<Beacon> beacons = new ArrayList<>();

	private final BeaconViewHolder.Listener beaconClickListener;

	public MyBeaconsAdapter(BeaconViewHolder.Listener beaconClickListener) {
		this.beaconClickListener = beaconClickListener;
	}

	public void setBeacons(List<Beacon> beacons) {
		this.beacons.clear();
		if (beacons != null) {
			this.beacons.addAll(beacons);
		}
		notifyDataSetChanged();
	}

	@Override
	public int getItemCount() {
		return beacons.size();
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new BeaconViewHolder(parent, beaconClickListener);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		Beacon beacon = beacons.get(position);
		if (beacon != null) {
			((BeaconViewHolder) holder).bind(beacon);
		}
	}

}