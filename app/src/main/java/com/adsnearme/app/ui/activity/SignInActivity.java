package com.adsnearme.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.R;
import com.adsnearme.app.api.Api;
import com.adsnearme.app.model.api.request.GetUserInfoRequest;
import com.adsnearme.app.utils.ButterKnifeUtils;
import com.adsnearme.app.utils.Validator;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class SignInActivity extends BaseLoginActivity {

	@BindView(R.id.edit_email)
	EditText editEmail;
	@BindView(R.id.edit_password)
	EditText editPassword;
	@BindView(R.id.button_sign_in)
	Button buttonSignIn;
	@BindView(R.id.button_forgot_password)
	View buttonForgotPassword;
	@BindView(R.id.button_facebook)
	View buttonFacebook;
	@BindView(R.id.button_sign_up)
	View buttonSignUp;
	@BindView(R.id.progress_bar)
	ProgressBar progressBar;

	@BindViews({R.id.edit_email, R.id.edit_password, R.id.button_sign_in, R.id.button_sign_up,
			R.id.button_facebook, R.id.button_forgot_password})
	List<View> clickableViews;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_in);
		ButterKnife.bind(this);
		checkSignInButton();
	}

	@Override
	public void showProgress() {
		ButterKnife.apply(clickableViews, ButterKnifeUtils.ENABLED, false);
		progressBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void hideProgress() {
		ButterKnife.apply(clickableViews, ButterKnifeUtils.ENABLED, true);
		progressBar.setVisibility(View.GONE);
	}

	@OnClick(R.id.button_sign_up)
	public void showSingUpActivity() {
		startActivity(new Intent(this, SignUpActivity.class));
		finish();
	}

	@OnTextChanged({R.id.edit_email, R.id.edit_password})
	public void checkSignInButton() {
		buttonSignIn.setEnabled(editEmail.length() > 0 && editPassword.length() > 0);
	}

	@OnClick(R.id.button_sign_in)
	public void signIn() {
		String email = editEmail.getText().toString().trim();
		String password = editPassword.getText().toString().trim();

		if (TextUtils.isEmpty(email)) {
			Toast.makeText(this, R.string.please_enter_email, Toast.LENGTH_SHORT).show();
			editEmail.requestFocus();
			editEmail.setSelection(editEmail.length());
			return;
		}
		if (Validator.isEmailInvalid(this, email)) {
			Toast.makeText(this, R.string.please_check_entered_email, Toast.LENGTH_SHORT).show();
			editEmail.requestFocus();
			editEmail.setSelection(editEmail.length());
			return;
		}
		if (TextUtils.isEmpty(password)) {
			Toast.makeText(this, R.string.please_enter_password, Toast.LENGTH_SHORT).show();
			editPassword.requestFocus();
			editPassword.setSelection(editPassword.length());
			return;
		}
		showProgress();

		AdsApp.component().auth().setAuthData(null);

		GetUserInfoRequest request = new GetUserInfoRequest();
		request.email = email;
		request.password = password;

		Api.getUserInfo(request)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(response -> onUserLoginSuccess(response, null), this::onUserLoginError);
	}

	@OnClick(R.id.button_facebook)
	public void facebookLogin() {
		showProgress();
		loginWithFacebook();
	}

}