package com.adsnearme.app.ui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

class InflaterViewHolder extends RecyclerView.ViewHolder {

	InflaterViewHolder(int layoutResId, ViewGroup parent) {
		super(LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false));
	}

}