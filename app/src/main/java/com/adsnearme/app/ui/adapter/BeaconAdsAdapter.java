package com.adsnearme.app.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.adsnearme.app.model.api.Ad;
import com.adsnearme.app.ui.adapter.viewholder.BeaconAdViewHolder;
import com.bumptech.glide.RequestManager;

import java.util.ArrayList;
import java.util.List;

public class BeaconAdsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private final List<Ad> ads = new ArrayList<>();

	private final BeaconAdViewHolder.Listener adClickListener;

	private RequestManager glide;

	public BeaconAdsAdapter(RequestManager glide, BeaconAdViewHolder.Listener adClickListener) {
		this.glide = glide;
		this.adClickListener = adClickListener;
	}

	public void setAds(List<Ad> ads) {
		this.ads.clear();
		if (ads != null) {
			this.ads.addAll(ads);
		}
		notifyDataSetChanged();
	}

	@Override
	public int getItemCount() {
		return ads.size();
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new BeaconAdViewHolder(parent, adClickListener);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		Ad ad = ads.get(position);
		if (ad != null) {
			((BeaconAdViewHolder) holder).bind(ad, glide);
		}
	}

}