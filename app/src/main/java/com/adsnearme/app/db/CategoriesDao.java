package com.adsnearme.app.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.adsnearme.app.model.Category;

import java.util.List;

@Dao
public interface CategoriesDao {

	@Query("SELECT * FROM " + Category.TABLE_NAME)
	LiveData<List<Category>> getCategories();

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void setCategories(List<Category> categories);

}