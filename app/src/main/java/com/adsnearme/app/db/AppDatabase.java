package com.adsnearme.app.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.adsnearme.app.model.Category;
import com.adsnearme.app.model.api.Ad;
import com.adsnearme.app.model.api.Beacon;

@Database(
		entities = {
				Beacon.class,
				Ad.class,
				Category.class
		},
		version = 1,
		exportSchema = false
)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

	public abstract BeaconsDao beacons();

	public abstract AdsDao ads();

	public abstract CategoriesDao categories();

}