package com.adsnearme.app.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.adsnearme.app.model.api.Ad;

import java.util.List;

@Dao
public interface AdsDao {

	@Query("SELECT * FROM " + Ad.TABLE_NAME + " WHERE " + Ad.COLUMN_BEACON_ID + " = :beaconId")
	LiveData<List<Ad>> getBeaconAds(String beaconId);

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void addAds(List<Ad> ads);

	@Query("DELETE FROM " + Ad.TABLE_NAME + " WHERE " + Ad.COLUMN_BEACON_ID + " = :beaconId")
	void deleteBeaconAds(String beaconId);

}