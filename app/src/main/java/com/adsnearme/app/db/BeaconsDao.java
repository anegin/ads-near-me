package com.adsnearme.app.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.adsnearme.app.model.api.Beacon;

import java.util.List;

@Dao
public interface BeaconsDao {

	@Query("SELECT * FROM " + Beacon.TABLE_NAME)
	LiveData<List<Beacon>> getBeacons();

	@Query("SELECT * FROM " + Beacon.TABLE_NAME + " WHERE " + Beacon.COLUMN_ID + " = :id")
	LiveData<Beacon> getBeacon(String id);

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void addBeacons(List<Beacon> beacons);

	@Update(onConflict = OnConflictStrategy.REPLACE)
	void updateBeacon(Beacon beacon);

	@Query("DELETE FROM " + Beacon.TABLE_NAME)
	void deleteAll();

}