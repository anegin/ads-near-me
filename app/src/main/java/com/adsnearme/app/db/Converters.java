package com.adsnearme.app.db;

import android.arch.persistence.room.TypeConverter;
import android.text.TextUtils;

import com.adsnearme.app.api.Api;
import com.adsnearme.app.model.api.Beacon;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Arrays;
import java.util.List;

public class Converters {

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(Api.DATETIME_FORMAT);

	@TypeConverter
	public static String dateTimeToString(DateTime dateTime) {
		return dateTime != null ? dateTimeFormatter.withZoneUTC().print(dateTime) : null;
	}

	@TypeConverter
	public static DateTime dateTimeFromString(String dateTimeString) {
		try {
			return dateTimeFormatter.withZoneUTC().parseDateTime(dateTimeString);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	// ===============

	@TypeConverter
	public static String stringsListToString(List<String> list) {
		return list != null ? TextUtils.join("|", list) : null;
	}

	@TypeConverter
	public static List<String> stringsListFromString(String str) {
		return str != null ? Arrays.asList(TextUtils.split(str, "\\|")) : null;
	}

	// ===============

	@TypeConverter
	public static String beaconTypeToString(Beacon.Type type) {
		return type != null ? type.name() : null;
	}

	@TypeConverter
	public static Beacon.Type beaconTypeFromString(String str) {
		if (str != null) {
			try {
				return Beacon.Type.valueOf(str);
			} catch (IllegalArgumentException ignored) {
			}
		}
		return null;
	}

}