package com.adsnearme.app.di;

import com.adsnearme.app.geofence.GeofenceUpdater;
import com.adsnearme.app.geofence.google.GoogleGeofenceUpdater;
import com.adsnearme.app.geofence.pathsense.PathsenseGeofenceUpdater;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class GeofenceModule {

	@Provides
	@Named("pathsense")
	GeofenceUpdater providePathsenseGeofenceUpdater() {
		return new PathsenseGeofenceUpdater();
	}

	@Provides
	@Named("google")
	GeofenceUpdater provideGoogleGeofenceUpdater() {
		return new GoogleGeofenceUpdater();
	}

}