package com.adsnearme.app.di;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;

import com.adsnearme.app.auth.Auth;
import com.adsnearme.app.db.AppDatabase;
import com.adsnearme.app.geofence.GeofenceUpdater;
import com.adsnearme.app.geofence.google.GoogleGeofenceUpdater;
import com.adsnearme.app.geofence.pathsense.PathsenseGeofenceUpdater;
import com.adsnearme.app.model.Categories;
import com.adsnearme.app.utils.Utils;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

	private final Application application;

	public AppModule(Application application) {
		this.application = application;
	}

	@Provides
	Context provideApplicationContext() {
		return application;
	}

	@Provides
	@Singleton
	@Named("device_id")
	String provideDeviceId(Context context) {
		return Utils.getDeviceId(context);
	}

	@Provides
	@Singleton
	Auth provideAuth() {
		return new Auth();
	}

	@Provides
	@Singleton
	SharedPreferences provideSharedPreferences(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	@Provides
	@Singleton
	ConnectivityManager provideConnectivityManager(Context context) {
		return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	}

	@Provides
	@Singleton
	AppDatabase provideAppDatabase(Context context) {
		return Room.databaseBuilder(context, AppDatabase.class, "db").build();
	}

	@Provides
	@Singleton
	Categories provideCategories() {
		return new Categories();
	}

}