package com.adsnearme.app.di;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

import com.adsnearme.app.auth.Auth;
import com.adsnearme.app.db.AppDatabase;
import com.adsnearme.app.geofence.UpdateGeofencesJob;
import com.adsnearme.app.model.Categories;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
		modules = {
				AppModule.class,
				GeofenceModule.class
		})
public interface AppComponent {

	Context appContext();

	@Named("device_id")
	String deviceId();

	Auth auth();

	SharedPreferences prefs();

	ConnectivityManager connectivityManager();

	AppDatabase database();

	Categories categories();

	void injectsUpdateGeofencesJob(UpdateGeofencesJob updateGeofencesJob);

}