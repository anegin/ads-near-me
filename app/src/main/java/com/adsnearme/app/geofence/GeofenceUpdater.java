package com.adsnearme.app.geofence;

import android.content.Context;

import com.adsnearme.app.model.api.Beacon;

import java.util.Collection;

public interface GeofenceUpdater {

	void updateBeaconGeofences(Context context, Collection<Beacon> beacons);

}