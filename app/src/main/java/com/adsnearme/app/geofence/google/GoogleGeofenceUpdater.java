package com.adsnearme.app.geofence.google;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.adsnearme.app.geofence.GeofenceUpdater;
import com.adsnearme.app.model.api.Beacon;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Tasks;

import java.util.Collection;
import java.util.concurrent.ExecutionException;

public class GoogleGeofenceUpdater implements GeofenceUpdater {

	private static final int LOITERING_DELAY = 10000;

	private static final String TAG = GoogleGeofenceUpdater.class.getSimpleName();

	@Override
	public void updateBeaconGeofences(Context context, Collection<Beacon> beacons) {
		Intent intent = new Intent(context, GeofenceTransitionService.class);
		PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		GeofencingRequest.Builder geofencingRequest = new GeofencingRequest.Builder()
				.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER
						| GeofencingRequest.INITIAL_TRIGGER_DWELL);
		for (Beacon beacon : beacons)
			geofencingRequest.addGeofence(new Geofence.Builder()
					.setRequestId(beacon.id)
					.setCircularRegion(beacon.latitude, beacon.longitude, beacon.geofenceRadius.floatValue())
					.setExpirationDuration(Geofence.NEVER_EXPIRE)
					.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER
							| Geofence.GEOFENCE_TRANSITION_DWELL)
					.setLoiteringDelay(LOITERING_DELAY)
					.build());

		GeofencingClient geofencingClient = LocationServices.getGeofencingClient(context);
		try {
			Log.v(TAG, "Deleting all geofences...");
			Tasks.await(geofencingClient.removeGeofences(pendingIntent));

			Log.v(TAG, "Adding " + beacons.size() + " geofence(s)...");
			Tasks.await(geofencingClient.addGeofences(geofencingRequest.build(), pendingIntent));

			Log.v(TAG, "Geofences updated successfully");
		} catch (ExecutionException | InterruptedException | SecurityException e) {
			Log.w(TAG, "Error updating geofences", e);
		}
	}

}