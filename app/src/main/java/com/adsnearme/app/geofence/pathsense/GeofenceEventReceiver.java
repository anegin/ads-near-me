package com.adsnearme.app.geofence.pathsense;

import android.content.Context;
import android.util.Log;

import com.adsnearme.app.geofence.GeofenceHelper;
import com.pathsense.android.sdk.location.PathsenseGeofenceEvent;
import com.pathsense.android.sdk.location.PathsenseGeofenceEventsReceiver;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GeofenceEventReceiver extends PathsenseGeofenceEventsReceiver {

	private static final String TAG = GeofenceEventReceiver.class.getSimpleName();

	@Override
	protected void onGeofenceEvents(Context context, List<PathsenseGeofenceEvent> events) {
		Log.v(TAG, "onGeofenceEvent: events=" + (events != null ? events.size() : 0));

		PathsenseGeofenceEvent nearestEvent = null;
		if (events != null && events.size() > 0) {
			for (PathsenseGeofenceEvent event : events) {
				if (event != null && event.isIngress() && (nearestEvent == null || nearestEvent.getRadius() > event.getRadius())) {
					nearestEvent = event;
				}
			}
		}
		if (nearestEvent != null) {
			String beaconId = nearestEvent.getGeofenceId();
			Log.v(TAG, "onGeofenceEvent: id=" + beaconId
					+ ", location=" + nearestEvent.getLatitude() + ", " + nearestEvent.getLongitude()
					+ ", radius=" + nearestEvent.getRadius());

			Observable.defer(() -> Observable.just(GeofenceHelper.loadGeofenceAdForBeacon(beaconId)))
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(
							geofenceAd -> GeofenceHelper.showGeofenceNotification(context, geofenceAd),
							t -> Log.v(TAG, "Error loading ads for beacon (id=" + beaconId + ")", t)
					);
		}
	}

}