package com.adsnearme.app.geofence.google;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.adsnearme.app.geofence.GeofenceHelper;
import com.adsnearme.app.model.api.response.ApiException;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;

import java.io.IOException;
import java.util.List;

public class GeofenceTransitionService extends IntentService {

	private static final String TAG = GeofenceTransitionService.class.getSimpleName();

	public GeofenceTransitionService() {
		super("[IntentService] " + TAG);
	}

	@Override
	protected void onHandleIntent(@Nullable Intent intent) {
		GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
		if (geofencingEvent == null) return;

		if (geofencingEvent.hasError()) {
			String errorMessage = GeofenceStatusCodes.getStatusCodeString(geofencingEvent.getErrorCode());
			Log.w(TAG, errorMessage);
			return;
		}
		int transition = geofencingEvent.getGeofenceTransition();
		switch (transition) {
			case Geofence.GEOFENCE_TRANSITION_ENTER:
				Log.v(TAG, "Geofences triggered: GEOFENCE_TRANSITION_ENTER");
				break;
			case Geofence.GEOFENCE_TRANSITION_DWELL:
				List<Geofence> geofences = geofencingEvent.getTriggeringGeofences();
				Log.v(TAG, "Geofences triggered: GEOFENCE_TRANSITION_DWELL, count=" + geofences.size());

				if (geofences.size() > 0) {
					String beaconId = geofences.get(0).getRequestId();
					try {
						GeofenceHelper.GeofenceAd geofenceAd = GeofenceHelper.loadGeofenceAdForBeacon(beaconId);
						GeofenceHelper.showGeofenceNotification(this, geofenceAd);
					} catch (ApiException | IOException e) {
						Log.v(TAG, "Error loading ads for beacon (id=" + beaconId + ")", e);
					}
				}
				break;
		}
	}

}