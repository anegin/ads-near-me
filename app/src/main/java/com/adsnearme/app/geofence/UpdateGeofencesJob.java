package com.adsnearme.app.geofence;

import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.model.AuthData;
import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.utils.LocationHelper;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Lazy;

public class UpdateGeofencesJob extends Job {

	public static final String JOB_TAG = "update_geofences_job";

	private static final String TAG = UpdateGeofencesJob.class.getSimpleName();

	private static final int MAX_GEOFENCES = 100;

	private static final int LOCATION_SEARCH_TIMEOUT = 10000;   // 10 sec
	private static final int SEARCH_RADIUS = 50000;             // 50 km

	@Inject
	@Named("pathsense")
	Lazy<GeofenceUpdater> geofenceUpdater;

	public UpdateGeofencesJob() {
		AdsApp.component().injectsUpdateGeofencesJob(this);
	}

	public static void schedule() {
		new JobRequest.Builder(UpdateGeofencesJob.JOB_TAG)
				.setPeriodic(TimeUnit.MINUTES.toMillis(30), TimeUnit.MINUTES.toMillis(15))
				.setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
				.setRequirementsEnforced(true)
				.setUpdateCurrent(true)
				.build()
				.schedule();
	}

	public static void startNow() {
		new JobRequest.Builder(UpdateGeofencesJob.JOB_TAG)
				.startNow()
				.build()
				.schedule();
	}

	@NonNull
	@Override
	protected Result onRunJob(Params params) {
		AuthData authData = AdsApp.component().auth().getAuthData();
		if (authData.authType == null) {
			Log.w(TAG, "User not authorized");
			return Result.SUCCESS;
		}

		if (!LocationHelper.hasPermission()) {
			Log.w(TAG, "Location permission denied");
			return Result.SUCCESS;
		}

		Log.v(TAG, "Requesting current location...");
		Location location;
		try {
			location = LocationHelper.requestCurrentLocation(getContext(), LOCATION_SEARCH_TIMEOUT);
		} catch (SecurityException e) {
			Log.w(TAG, "Can't get current location: permission denied");
			return Result.SUCCESS;
		}
		if (location == null) {
			Log.w(TAG, "Can't get current location: location not received in "
					+ LOCATION_SEARCH_TIMEOUT + " ms");
			return Result.SUCCESS;
		}

		Collection<Beacon> beacons = GeofenceHelper.loadBeacons(location, SEARCH_RADIUS, MAX_GEOFENCES);
		geofenceUpdater.get().updateBeaconGeofences(getContext(), beacons);

		return Result.SUCCESS;
	}

}