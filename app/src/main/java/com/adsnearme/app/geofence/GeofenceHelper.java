package com.adsnearme.app.geofence;

import android.content.Context;
import android.graphics.Typeface;
import android.location.Location;
import android.support.annotation.NonNull;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;

import com.adsnearme.app.R;
import com.adsnearme.app.api.ApiHelper;
import com.adsnearme.app.arch.paging.AdsDataSource;
import com.adsnearme.app.model.api.Ad;
import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.model.api.request.GetAdListRequest;
import com.adsnearme.app.model.api.request.GetGeofenceAdRequest;
import com.adsnearme.app.model.api.response.ApiException;
import com.adsnearme.app.model.api.response.GetGeofenceAdResponse;
import com.adsnearme.app.utils.NotificationUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeofenceHelper {

	private static final String TAG = GeofenceHelper.class.getSimpleName();

	private static final int NOTIFICATION_ID = 42;

	static Collection<Beacon> loadBeacons(@NonNull Location location, int radius, int maxCount) {
		Log.v(TAG, "Requesting ads for location: " + location.getLatitude() + " " + location.getLongitude()
				+ " in + " + radius + " m radius");

		GetAdListRequest request = new GetAdListRequest();
		request.latitude = location.getLatitude();
		request.longitude = location.getLongitude();
		request.radius = radius;

		AdsDataSource adsDataSource = new AdsDataSource(request);
		int offs = 0;
		int limit = maxCount;
		int receivedAdsCount = 0;
		Map<String, Beacon> beacons = new HashMap<>();
		while (beacons.size() < maxCount) {
			List<Ad> ads = adsDataSource.loadRange(offs, limit);
			if (ads == null || ads.size() == 0) break;
			for (Ad ad : ads) {
				if (ad != null && ad.beaconId != null) {
					Beacon beacon = beacons.get(ad.beaconId);
					if (beacon == null) {
						if (ad.latitude != null && ad.latitude != 0
								&& ad.longitude != null && ad.longitude != 0
								&& ad.geofenceRadius != null && ad.geofenceRadius != 0) {
							beacon = new Beacon();
							beacon.id = ad.beaconId;
							beacon.latitude = ad.latitude;
							beacon.longitude = ad.longitude;
							beacon.geofenceRadius = ad.geofenceRadius;
							beacons.put(ad.beaconId, beacon);
						}
					} else if (ad.geofenceRadius != null && ad.geofenceRadius > beacon.geofenceRadius) {
						beacon.geofenceRadius = ad.geofenceRadius;
					}
				}
			}
			receivedAdsCount += ads.size();
			int totalAdsCount = adsDataSource.countItems();
			if (totalAdsCount >= 0 && receivedAdsCount < totalAdsCount) {
				offs += ads.size();
				limit = Math.min(maxCount, totalAdsCount - receivedAdsCount);
			} else {
				break;
			}
		}
		return beacons.values();
	}

	@NonNull
	public static GeofenceAd loadGeofenceAdForBeacon(String beaconId) throws ApiException, IOException {
		GetGeofenceAdResponse response = ApiHelper.execute(new GetGeofenceAdRequest(beaconId), GetGeofenceAdResponse.class);
		if (response.answer.ads == null || response.answer.ads.size() == 0)
			throw new IOException("No ads in response");
		Ad ad = response.answer.ads.get(0);
		int count = response.answer.adsCount != null ? response.answer.adsCount : response.answer.ads.size();
		return new GeofenceAd(ad, count);
	}

	public static void showGeofenceNotification(Context context, GeofenceAd geofenceAd) {
		String title = context.getString(R.string.geofence_notification_title, geofenceAd.adsCount);

		String message = geofenceAd.firstAd.name;
		if (!TextUtils.isEmpty(geofenceAd.firstAd.name)) {
			message += ". " + geofenceAd.firstAd.description;
		}

		SpannableStringBuilder bigText = new SpannableStringBuilder(geofenceAd.firstAd.name);
		bigText.setSpan(new StyleSpan(Typeface.BOLD), 0, bigText.length(), SpannableStringBuilder.SPAN_INCLUSIVE_EXCLUSIVE);
		if (!TextUtils.isEmpty(geofenceAd.firstAd.description)) {
			bigText.append('\n').append(geofenceAd.firstAd.description);
		}

		NotificationUtils.showNotification(context, title, message, bigText, geofenceAd.adsCount, NOTIFICATION_ID);
	}

	// =============

	public static class GeofenceAd {

		final Ad firstAd;
		final int adsCount;

		GeofenceAd(Ad firstAd, int adsCount) {
			this.firstAd = firstAd;
			this.adsCount = adsCount;
		}

	}

}