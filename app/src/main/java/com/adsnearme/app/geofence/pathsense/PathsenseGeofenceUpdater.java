package com.adsnearme.app.geofence.pathsense;

import android.content.Context;
import android.util.Log;

import com.adsnearme.app.geofence.GeofenceUpdater;
import com.adsnearme.app.model.api.Beacon;
import com.pathsense.android.sdk.location.PathsenseGeofenceEventEnum;
import com.pathsense.android.sdk.location.PathsenseLocationProviderApi;

import java.util.Collection;

public class PathsenseGeofenceUpdater implements GeofenceUpdater {

	private static final String TAG = PathsenseGeofenceUpdater.class.getSimpleName();

	@Override
	public void updateBeaconGeofences(Context context, Collection<Beacon> beacons) {
		PathsenseLocationProviderApi api = PathsenseLocationProviderApi.getInstance(context);

		Log.v(TAG, "Deleting all geofences...");
		api.removeGeofences();

		Log.v(TAG, "Adding " + beacons.size() + " geofence(s)...");
		for (Beacon beacon : beacons)
			api.addGeofence(beacon.id,
					beacon.latitude,
					beacon.longitude,
					beacon.geofenceRadius.intValue(),
					PathsenseGeofenceEventEnum.INGRESS,
					GeofenceEventReceiver.class);

		Log.v(TAG, "Geofences updated successfully");
	}

}