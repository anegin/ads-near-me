package com.adsnearme.app.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.UUID;

public class Utils {

	@SuppressLint("HardwareIds")
	public static String getDeviceId(Context context) {
		String androidId = "" + Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
		return "android:" + new UUID(Build.SERIAL.hashCode(), androidId.hashCode()).toString();
	}

	public static boolean objectEquals(Object a, Object b) {
		return (a == b) || (a != null && a.equals(b));
	}

	public static float dpToPx(Context context, float dp) {
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
	}

	public static void setKeyboardVisible(Context context, View view, boolean visible) {
		if (context == null || view == null) return;
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null) {
			if (visible) {
				imm.showSoftInput(view, 0);
			} else {
				imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			}
		}
	}

}