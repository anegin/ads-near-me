package com.adsnearme.app.utils;

import android.content.Context;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.R;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

	public static String formatDateShort(DateTime dateTime) {
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
		return dateFormat.format(new Date(dateTime.getMillis()));
	}

	public static String formatDateLong(DateTime dateTime) {
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
		return dateFormat.format(new Date(dateTime.getMillis()));
	}

	public static String formatPeriod(long minutes) {
		int hoursPerDay = 24;
		int minutesPerHour = 60;
		int minutesPerDay = hoursPerDay * minutesPerHour;
		long days = minutes / minutesPerDay;
		minutes -= days * minutesPerDay;
		long hours = minutes / minutesPerHour;
		minutes -= hours * minutesPerHour;
		Context context = AdsApp.component().appContext();
		StringBuilder sb = new StringBuilder();
		if (days > 0) {
			sb.append(days).append(' ').append(context.getString(R.string.days_short));
		}
		if (hours > 0) {
			if (sb.length() > 0) sb.append(' ');
			sb.append(hours).append(' ').append(context.getString(R.string.hours_short));
		}
		if (sb.length() > 0) sb.append(' ');
		sb.append(minutes).append(' ').append(context.getString(R.string.minutes_short));
		return sb.toString();
	}

}
