package com.adsnearme.app.utils.glide;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Build;
import android.support.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.TransformationUtils;
import com.bumptech.glide.util.Preconditions;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RoundedCornersTransformation extends BitmapTransformation {

	private static final String ID = RoundedCornersTransformation.class.getName();
	private static final byte[] ID_BYTES = ID.getBytes(CHARSET);

	private final int roundingRadius;

	public RoundedCornersTransformation(int roundingRadius) {
		Preconditions.checkArgument(roundingRadius > 0, "roundingRadius must be greater than 0.");
		this.roundingRadius = roundingRadius;
	}

	private static Bitmap getAlphaSafeBitmap(@NonNull BitmapPool pool, @NonNull Bitmap maybeAlphaSafe) {
		if (Bitmap.Config.ARGB_8888.equals(maybeAlphaSafe.getConfig())) {
			return maybeAlphaSafe;
		} else {
			Bitmap argbBitmap = pool.get(maybeAlphaSafe.getWidth(), maybeAlphaSafe.getHeight(), Bitmap.Config.ARGB_8888);
			(new Canvas(argbBitmap)).drawBitmap(maybeAlphaSafe, 0.0F, 0.0F, null);
			return argbBitmap;
		}
	}

	protected Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap inBitmap, int width, int height) {
		Preconditions.checkArgument(width > 0, "width must be greater than 0.");
		Preconditions.checkArgument(height > 0, "height must be greater than 0.");
		Preconditions.checkArgument(roundingRadius > 0, "roundingRadius must be greater than 0.");

		Bitmap resizedBitmap = TransformationUtils.fitCenter(pool, inBitmap, width, height);

		Bitmap toTransform = getAlphaSafeBitmap(pool, resizedBitmap);

		Bitmap result = pool.get(width, height, Bitmap.Config.ARGB_8888);
		result.setHasAlpha(true);

		BitmapShader shader = new BitmapShader(toTransform, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setShader(shader);
		paint.setFilterBitmap(true);
		RectF rect = new RectF(0.0F, 0.0F, (float) result.getWidth(), (float) result.getHeight());

		BITMAP_DRAWABLE_LOCK.lock();
		try {
			Canvas canvas = new Canvas(result);
			canvas.drawColor(0, PorterDuff.Mode.CLEAR);
			canvas.drawRoundRect(rect, (float) roundingRadius, (float) roundingRadius, paint);
			canvas.setBitmap(null);
		} finally {
			BITMAP_DRAWABLE_LOCK.unlock();
		}

		if (!toTransform.equals(resizedBitmap)) {
			pool.put(toTransform);
		}

		return result;
	}

	public boolean equals(Object o) {
		return o instanceof RoundedCornersTransformation
				&& ((RoundedCornersTransformation) o).roundingRadius == this.roundingRadius;
	}

	public int hashCode() {
		return ID.hashCode() + this.roundingRadius;
	}

	public void updateDiskCacheKey(MessageDigest messageDigest) {
		messageDigest.update(ID_BYTES);
		byte[] radiusData = ByteBuffer.allocate(4).putInt(this.roundingRadius).array();
		messageDigest.update(radiusData);
	}

	// ==================

	private static final List<String> MODELS_REQUIRING_BITMAP_LOCK = Arrays.asList("XT1097", "XT1085");
	private static final Lock BITMAP_DRAWABLE_LOCK = (Lock) (MODELS_REQUIRING_BITMAP_LOCK.contains(Build.MODEL) && Build.VERSION.SDK_INT == 22 ? new ReentrantLock() : new NoLock());

	private static final class NoLock implements Lock {
		NoLock() {
		}

		public void lock() {
		}

		public void lockInterruptibly() throws InterruptedException {
		}

		public boolean tryLock() {
			return true;
		}

		public boolean tryLock(long time, @NonNull TimeUnit unit) throws InterruptedException {
			return true;
		}

		public void unlock() {
		}

		@NonNull
		public Condition newCondition() {
			throw new UnsupportedOperationException("Should not be called");
		}
	}

}