package com.adsnearme.app.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.adsnearme.app.api.Api;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class GsonUtils {

	public static final Gson GSON = new GsonBuilder()
			.registerTypeAdapter(DateTime.class, new JodaUTCDateTimeAdapter(Api.DATETIME_FORMAT))
			.create();

	@NonNull
	public static Map<String, String> objectToMap(@Nullable Object obj) {
		if (obj != null) {
			String requestJson = GSON.toJson(obj);
			Type stringStringMapType = new TypeToken<Map<String, String>>() {
			}.getType();
			return GSON.fromJson(requestJson, stringStringMapType);
		}
		return new HashMap<>();
	}

	public static <T> T copyObject(T source, Class<T> tClass) {
		if (source == null) return null;
		return GSON.fromJson(GSON.toJson(source), tClass);
	}

	private static class JodaUTCDateTimeAdapter implements JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

		private final DateTimeFormatter dateTimeFormatter;

		JodaUTCDateTimeAdapter(String dateTimeFormat) {
			dateTimeFormatter = DateTimeFormat.forPattern(dateTimeFormat);
		}

		@Override
		public synchronized JsonElement serialize(DateTime date, Type type, JsonSerializationContext jsonSerializationContext) {
			if (date == null) return null;
			if (date.getMillis() == 0) return JsonNull.INSTANCE;
			return new JsonPrimitive(dateTimeFormatter.withZoneUTC().print(date));
		}

		@Override
		public synchronized DateTime deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
			if (jsonElement.isJsonNull()) return null;
			try {
				String dateTimeString = jsonElement.getAsString();
				return dateTimeFormatter.withZoneUTC().parseDateTime(dateTimeString);
			} catch (ClassCastException | IllegalStateException e) {
				throw new JsonParseException("Date must be primitive type (string)");
			} catch (IllegalArgumentException e) {
				throw new JsonParseException("Invalid date format");
			}
		}

	}

}