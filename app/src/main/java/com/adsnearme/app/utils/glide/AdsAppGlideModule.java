package com.adsnearme.app.utils.glide;

import android.content.Context;
import android.util.Log;

import com.adsnearme.app.BuildConfig;
import com.adsnearme.app.utils.network.Network;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

import java.io.InputStream;

import okhttp3.OkHttpClient;

@GlideModule
public class AdsAppGlideModule extends AppGlideModule {

	@Override
	public void applyOptions(Context context, GlideBuilder builder) {
		builder.setDefaultRequestOptions(new RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
				.setMemoryCache(new LruResourceCache(50 * 1024 * 1024))
				.setLogLevel(BuildConfig.DEBUG ? Log.VERBOSE : Log.WARN)
				.setDiskCache(new InternalCacheDiskCacheFactory(context, 100 * 1024 * 100));
	}

	@Override
	public void registerComponents(Context context, Glide glide, Registry registry) {
		OkHttpClient okHttpClient = Network.getOkHttpClient();
		registry.replace(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(okHttpClient));
	}

	@Override
	public boolean isManifestParsingEnabled() {
		return false;
	}

}