package com.adsnearme.app.utils.network;

import android.os.Build;

import java.io.IOException;

import com.adsnearme.app.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

class UserAgentInterceptor implements Interceptor {

	private final String userAgent;

	UserAgentInterceptor() {
		this.userAgent = BuildConfig.APPLICATION_ID
				+ "/v" + BuildConfig.VERSION_NAME + "(" + BuildConfig.VERSION_CODE + "); "
				+ Build.MANUFACTURER + " " + Build.MODEL + "; "
				+ "Android " + Build.VERSION.RELEASE + " (API" + Build.VERSION.SDK_INT + ")";
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		Request request = chain.request().newBuilder()
				.addHeader("User-Agent", userAgent)
				.build();
		return chain.proceed(request);
	}

}