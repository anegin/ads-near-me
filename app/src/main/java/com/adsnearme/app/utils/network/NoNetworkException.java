package com.adsnearme.app.utils.network;

import java.io.IOException;

public class NoNetworkException extends IOException {

	public NoNetworkException() {
		super("No network connection");
	}

}