package com.adsnearme.app.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import io.reactivex.subjects.BehaviorSubject;

public class LocationHelper {

	private static final double METERS_PER_MILE = 1609.344;

	public static boolean hasPermission() {
		Context appContext = AdsApp.component().appContext();
		return ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
				|| ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
	}

	public static String toDegreesString(double latitude, double longitude) {
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("###.#####");
		df.setDecimalFormatSymbols(dfs);
		return (latitude < 0 ? "S" : "N") + df.format(Math.abs(latitude)) +
				(longitude < 0 ? " W" : " E") + df.format(Math.abs(longitude));
	}

	public static double milesToMeters(double miles) {
		return miles * METERS_PER_MILE;
	}

	public static String distanceToString(double distanceInMeters) {
		Context appContext = AdsApp.component().appContext();
		if (distanceInMeters < 1000) {
			return appContext.getString(R.string.dimension_meters, String.valueOf((int) distanceInMeters));
		} else {
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator('.');
			DecimalFormat df = new DecimalFormat("#.#");
			df.setDecimalFormatSymbols(dfs);
			return appContext.getString(R.string.dimension_kilometers, df.format(distanceInMeters / 1000));
		}
	}

	public static Location requestCurrentLocation(Context context, long maxWaitTimeMs) throws SecurityException {
		long startTime = SystemClock.uptimeMillis();

		FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

		BehaviorSubject<Location> locationSubj = BehaviorSubject.create();

		CountDownLatch waiter = new CountDownLatch(1);

		Task<Location> getLastLocationTask = fusedLocationClient.getLastLocation()
				.addOnCompleteListener(task -> waiter.countDown());

		try {
			waiter.await(maxWaitTimeMs, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			return locationSubj.getValue();
		}

		if (getLastLocationTask.isComplete() && getLastLocationTask.isSuccessful()) {
			Location location = getLastLocationTask.getResult();
			if (location != null) {
				locationSubj.onNext(location);
			}
		}

		long elapsedTime = SystemClock.uptimeMillis() - startTime;
		if (elapsedTime < maxWaitTimeMs) {
			// если еще осталось время, подписываемся на обновления местоположения
			long remainingTime = maxWaitTimeMs - elapsedTime;

			LocationRequest locationRequest = new LocationRequest()
					.setExpirationDuration(remainingTime)
					.setMaxWaitTime(remainingTime)
					.setInterval(1000)
					.setFastestInterval(500)
					.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

			LocationCallback locationCallback = new LocationCallback() {
				@Override
				public void onLocationResult(LocationResult locationResult) {
					Location location = locationResult.getLastLocation();
					if (location != null) {
						locationSubj.onNext(location);
					}
				}
			};

			Task<Void> locationUpdatesTask = null;
			HandlerThread locationHandlerThread = new HandlerThread("MyHandlerThread");
			try {
				locationHandlerThread.start();
				locationUpdatesTask = fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, locationHandlerThread.getLooper());
				Thread.sleep(remainingTime);
			} catch (InterruptedException ignored) {
			} finally {
				if (locationUpdatesTask != null) {
					fusedLocationClient.removeLocationUpdates(locationCallback);
				}
				locationHandlerThread.quit();
			}
		}
		return locationSubj.getValue();
	}

}