package com.adsnearme.app.utils;

import android.view.View;

import butterknife.ButterKnife;

public class ButterKnifeUtils {

	public static final ButterKnife.Action<View> DISABLE = (view, index) -> view.setEnabled(false);

	public static final ButterKnife.Setter<View, Boolean> ENABLED = (view, value, index) -> view.setEnabled(value);

}
