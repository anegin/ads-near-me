package com.adsnearme.app.utils.network;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.concurrent.TimeUnit;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;

public class Network {

	private static final int CONNECT_TIMEOUT = 10000;
	private static final int READ_TIMEOUT = 20000;
	private static final int WRITE_TIMEOUT = 10000;

	private static Interceptor USER_AGENT_INTERCEPTOR = new UserAgentInterceptor();
	private static Interceptor LOGGING_INTERCEPTOR = new LoggingInterceptor(BuildConfig.DEBUG);

	public static OkHttpClient getOkHttpClient() {
		return new OkHttpClient.Builder()
				.connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
				.readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
				.writeTimeout(WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
				.addInterceptor(USER_AGENT_INTERCEPTOR)
				.addInterceptor(LOGGING_INTERCEPTOR)
				.build();
	}

	public static boolean isNetworkAvailable() {
		ConnectivityManager cm = AdsApp.component().connectivityManager();
		if (cm == null) return false;
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		return networkInfo != null && networkInfo.isConnected();
	}

}