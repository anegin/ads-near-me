package com.adsnearme.app.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.adsnearme.app.BuildConfig;
import com.adsnearme.app.R;
import com.adsnearme.app.ui.activity.StartActivity;

public class NotificationUtils {

	private static final String CHANNEL_ID = BuildConfig.APPLICATION_ID;
	private static final String CHANNEL_NAME = "geofence";

	private static final long[] VIBRATE_PATTERN = new long[]{0, 75, 75, 75};

	public static void showNotification(Context context,
	                                    String title,
	                                    String message,
	                                    CharSequence bigText,
	                                    int count,
	                                    int notificationId) {

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		if (notificationManager == null) return;

		Notification.Builder notificationBuilder;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = notificationManager.getNotificationChannel(CHANNEL_ID);
			if (channel == null) {
				channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
				channel.enableLights(false);
				channel.enableVibration(true);
				channel.setVibrationPattern(VIBRATE_PATTERN);
				notificationManager.createNotificationChannel(channel);
			}
			notificationBuilder = new Notification.Builder(context, CHANNEL_ID);
		} else {
			notificationBuilder = new Notification.Builder(context);
			notificationBuilder.setVibrate(VIBRATE_PATTERN);
		}

		Intent contentIntent = new Intent(context, StartActivity.class);
		PendingIntent contentPendingIntent = PendingIntent.getActivity(context, 0, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		notificationBuilder
				.setContentTitle(title)
				.setContentText(message)
				.setContentIntent(contentPendingIntent)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setWhen(System.currentTimeMillis())
				.setNumber(count)
				.setAutoCancel(true)
				.setOngoing(false);

		if (!TextUtils.isEmpty(bigText)) {
			Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle()
					.setBigContentTitle(title)
					.bigText(bigText);
			notificationBuilder.setStyle(bigTextStyle);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			notificationBuilder
					.setColor(ContextCompat.getColor(context, R.color.color_accent))
					.setCategory(NotificationCompat.CATEGORY_EVENT);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			notificationBuilder.setColorized(true);
		}

		notificationManager.notify(notificationId, notificationBuilder.build());
	}

}