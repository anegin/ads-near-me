package com.adsnearme.app.model.api.response;

import com.google.gson.annotations.SerializedName;

public class ApiException extends Exception {

	// пользователь с таким email уже зарегистрирован (при регистрации)
	public static final String USER_ALREADY_EXISTS = "00005";

	// не найден пользователь с таким логином и/или паролем (при авторизации)
	public static final String USER_NOT_EXISTS = "00011";

	// у текущего пользователя нет биконов (при получении списка своих биконов)
	public static final String USER_HAVE_NO_BEACONS = "00013";

	// ничего не найдено (при получении списка биконов, списка объявлений)
	public static final String EMPTY_LIST = "00031";

	@SerializedName("error_code")
	public String code;

	@SerializedName("error_text")
	public String text;

	public ApiException(ApiAnswer apiAnswer) {
		if (apiAnswer != null) {
			this.code = apiAnswer.errorCode;
			this.text = apiAnswer.errorText;
		} else {
			this.code = "-1";
			this.text = "Empty error answer";
		}
	}

}