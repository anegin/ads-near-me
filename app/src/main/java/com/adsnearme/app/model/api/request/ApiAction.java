package com.adsnearme.app.model.api.request;

import com.google.gson.annotations.SerializedName;

public enum ApiAction {

	@SerializedName("reg_user")
	REG_USER,                               // регистрация юзера

	@SerializedName("get_user_info")
	GET_USER_INFO,                          // авторизация юзера по email/pass или через соцсеть

	@SerializedName("get_user_beacons")
	GET_USER_BEACONS,                       // получение списка биконов пользователя

	@SerializedName("get_beacons_ads")
	GET_BEACON_ADS,                         // получение списка объявлений, прикрепленных к бикону

	@SerializedName("manage_beacons")
	MANAGE_BEACONS,                         // получение списка объявлений, прикрепленных к бикону

	@SerializedName("get_app_info")
	GET_APP_INFO,                           // получение списка категорий/подкатегорий

	@SerializedName("get_ad_list")
	GET_AD_LIST,                            // получение списка объявлений

	@SerializedName("manage_favorites")
	MANAGE_FAVORITES,                       // добавление/удаление объявления из избранного

	@SerializedName("get_beacons_offers_geofencing")
	GET_BEACONS_OFFERS_GEOFENCING           // добавление/удаление объявления из избранного

}