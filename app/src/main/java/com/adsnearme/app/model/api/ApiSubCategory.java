package com.adsnearme.app.model.api;

import com.google.gson.annotations.SerializedName;

public class ApiSubCategory {

	public String id;

	@SerializedName("s_name")
	public String name;

	@SerializedName("s_addit_filters")
	public boolean additionalFilters;

}