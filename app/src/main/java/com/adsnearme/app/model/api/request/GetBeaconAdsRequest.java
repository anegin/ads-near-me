package com.adsnearme.app.model.api.request;

import com.adsnearme.app.model.api.Beacon;
import com.google.gson.annotations.SerializedName;

public class GetBeaconAdsRequest extends ApiRequest {

	@SerializedName("beacon_uuid")
	public String beaconUuid;

	@SerializedName("major_id")
	public String majorId;

	@SerializedName("minor_id")
	public String minorId;

	public GetBeaconAdsRequest(Beacon beacon) {
		super(ApiAction.GET_BEACON_ADS);
		this.beaconUuid = beacon.uuid;
		this.majorId = beacon.major;
		this.minorId = beacon.minor;
	}

}