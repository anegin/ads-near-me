package com.adsnearme.app.model.api.request;

public class GetUserBeaconsRequest extends ApiRequest {

	public GetUserBeaconsRequest() {
		super(ApiAction.GET_USER_BEACONS);
	}

}