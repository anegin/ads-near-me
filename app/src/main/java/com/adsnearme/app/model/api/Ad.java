package com.adsnearme.app.model.api;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.List;

@Entity(tableName = Ad.TABLE_NAME)
public class Ad {

	public static final String TABLE_NAME = "ads";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_BEACON_ID = "beacon_id";

	@NonNull
	@PrimaryKey
	@ColumnInfo(name = COLUMN_ID)
	@SerializedName("a_id")
	public String id = "";

	@SerializedName("a_category")
	public String category;

	@SerializedName("a_subcategory")
	@ColumnInfo(name = "sub_category")
	public String subCategory;

	@SerializedName("a_price")
	public Double price;

	@SerializedName("a_name")
	public String name;

	@SerializedName("a_description")
	public String description;

	@SerializedName("a_site_url")
	@ColumnInfo(name = "site_url")
	public String siteUrl;

	@SerializedName("a_youtube_url")
	@ColumnInfo(name = "youtube_url")
	public String youtubeUrl;

	@SerializedName("a_color")
	public String color;

	@SerializedName("a_images")
	public List<String> images;

	@SerializedName("a_images_little")
	@ColumnInfo(name = "images_little")
	public List<String> imagesLittle;

	@SerializedName("a_date_add")
	@ColumnInfo(name = "date_added")
	public DateTime dateAdded;

	@SerializedName("a_date_upd")
	@ColumnInfo(name = "date_updated")
	public DateTime dateUpdated;

	@SerializedName("a_date_end")
	@ColumnInfo(name = "date_end")
	public DateTime dateEnd;

	@SerializedName("b_latitude")
	public Double latitude;

	@SerializedName("b_longitude")
	public Double longitude;

	@SerializedName("distance")
	public Double distance;

	@SerializedName("a_addit_field_1")
	@ColumnInfo(name = "additional_field_1")
	public String additionalField1;

	@SerializedName("a_addit_field_2")
	@ColumnInfo(name = "additional_field_2")
	public String additionalField2;

	@SerializedName("a_addit_field_3")
	@ColumnInfo(name = "additional_field_3")
	public String additionalField3;


	@SerializedName("a_is_active")
	@ColumnInfo(name = "is_active")
	public int isActive;

	@SerializedName("in_favorites")
	@ColumnInfo(name = "in_favorites")
	public int inFavorites;


	@SerializedName("a_rating")
	public Double rating;

	@SerializedName("a_price_position")
	@ColumnInfo(name = "price_position")
	public String pricePosition;

	@SerializedName("a_preview_form")
	@ColumnInfo(name = "preview_form")
	public String previewForm;


	@SerializedName("a_user_id")
	@ColumnInfo(name = "user_id")
	public String userId;

	@SerializedName("u_name")
	@ColumnInfo(name = "user_name")
	public String userName;

	@SerializedName("u_surname")
	@ColumnInfo(name = "user_surname")
	public String userSurname;


	@SerializedName("beacon_id")
	@ColumnInfo(name = COLUMN_BEACON_ID)
	public String beaconId;

	@SerializedName("b_geofencing_radius")
	@ColumnInfo(name = "geofence_radius")
	public Double geofenceRadius;

	// ======

	public String getImageUrl() {
		String imageUrl = imagesLittle != null && imagesLittle.size() > 0 ? imagesLittle.get(0) : null;
		if (imageUrl == null || imageUrl.length() == 0) {
			imageUrl = images != null && images.size() > 0 ? images.get(0) : null;
		}
		return imageUrl;
	}

}