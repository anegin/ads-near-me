package com.adsnearme.app.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class ApiCategory {

	public String id;

	@SerializedName("c_name")
	public String name;

	@SerializedName("c_addit_filters")
	public boolean additionalFilters;

	public Map<String, ApiSubCategory> subcategories;

}