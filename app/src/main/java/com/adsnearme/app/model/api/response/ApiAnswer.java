package com.adsnearme.app.model.api.response;

import com.google.gson.annotations.SerializedName;

public class ApiAnswer {

	public enum Type {

		@SerializedName("ok")
		SUCCESS,

		@SerializedName("err")
		ERROR

	}

	@SerializedName("error_code")
	public String errorCode;

	@SerializedName("error_text")
	public String errorText;

}