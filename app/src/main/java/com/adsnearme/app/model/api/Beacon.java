package com.adsnearme.app.model.api;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.List;

@Entity(tableName = Beacon.TABLE_NAME)
public class Beacon {

	public enum Type {
		@SerializedName("0")
		STATIONARY,
		@SerializedName("1")
		MOVING
	}

	public static final String TABLE_NAME = "beacons";
	public static final String COLUMN_ID = "id";

	@NonNull
	@PrimaryKey
	@ColumnInfo(name = COLUMN_ID)
	public String id = "";

	@SerializedName("b_beacon_uuid")
	public String uuid;

	@SerializedName("b_major_id")
	public String major;

	@SerializedName("b_minor_id")
	public String minor;

	@SerializedName("b_latitude")
	public Double latitude;                     // nullable

	@SerializedName("b_longitude")
	public Double longitude;                    // nullable

	@SerializedName("b_date_add")
	@ColumnInfo(name = "date_added")
	public DateTime dateAdded;

	@SerializedName("b_color")
	public String color;                        // "#11aaff"

	@SerializedName("b_name")
	public String name;

	@SerializedName("b_type")
	public Type type;

	@SerializedName("b_geofencing_radius")
	@ColumnInfo(name = "geofence_radius")
	public Double geofenceRadius;               // nullable

	@SerializedName("beacon_ads")
	public List<String> ads;                    // список idшников объявлений

}