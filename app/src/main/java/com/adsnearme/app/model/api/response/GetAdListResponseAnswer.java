package com.adsnearme.app.model.api.response;

import com.adsnearme.app.model.api.Ad;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAdListResponseAnswer extends ApiAnswer {

	@SerializedName("ad_info")
	public List<Ad> ads;

	@SerializedName("ad_count")
	public Integer adsCount;

}