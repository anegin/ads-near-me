package com.adsnearme.app.model.api.request;

import com.adsnearme.app.AdsApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.annotations.SerializedName;

public class RegUserRequest extends ApiRequest {

	public String email;
	public String password;

	public String name;
	public String surname;

	@SerializedName("auth_device_id")
	public final String authDeviceId;           // deviceId

	@SerializedName("android_device_id")
	public final String firebaseDeviceToken;    // id девайса для пушей

	public RegUserRequest(String email, String password, String name, String surname) {
		super(ApiAction.REG_USER);
		this.authDeviceId = AdsApp.component().deviceId();
		this.firebaseDeviceToken = FirebaseInstanceId.getInstance().getToken();
		this.email = email;
		this.password = password;
		this.name = name;
		this.surname = surname;
	}

}