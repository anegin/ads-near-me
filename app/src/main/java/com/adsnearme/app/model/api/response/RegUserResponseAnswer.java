package com.adsnearme.app.model.api.response;

import com.google.gson.annotations.SerializedName;

public class RegUserResponseAnswer extends ApiAnswer {

	@SerializedName("u_id")
	public String userId;

}