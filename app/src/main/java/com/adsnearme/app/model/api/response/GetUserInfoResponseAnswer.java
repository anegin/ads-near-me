package com.adsnearme.app.model.api.response;

import com.adsnearme.app.model.api.User;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

public class GetUserInfoResponseAnswer extends ApiAnswer {

	@SerializedName("u_id")
	public String userId;

	@SerializedName("u_email")
	public String userEmail;

	@SerializedName("u_fb_id")
	public String userFacebookId;

	@SerializedName("u_name")
	public String userName;

	@SerializedName("u_surname")
	public String userSurname;

	@SerializedName("u_paid_to")
	public DateTime userPaidTo;

	@SerializedName("ad_count")
	public int adCount;

	public User getUser() {
		User user = new User();
		user.id = userId;
		user.email = userEmail;
		user.facebookUid = userFacebookId;
		user.name = userName;
		user.surname = userSurname;
		user.paidTo = userPaidTo;
		user.adCount = adCount;
		return user;
	}

}