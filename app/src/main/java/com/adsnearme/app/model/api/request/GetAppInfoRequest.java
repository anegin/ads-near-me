package com.adsnearme.app.model.api.request;

public class GetAppInfoRequest extends ApiRequest {

	public GetAppInfoRequest() {
		super(ApiAction.GET_APP_INFO);
	}

}