package com.adsnearme.app.model.api;

import org.joda.time.DateTime;

public class User {

	public String id;

	public String email;

	public String facebookUid;

	public String name;

	public String surname;

	public DateTime paidTo;

	public int adCount;

}
