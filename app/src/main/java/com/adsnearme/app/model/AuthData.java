package com.adsnearme.app.model;

import com.adsnearme.app.auth.AuthType;
import com.adsnearme.app.model.api.User;

public class AuthData {

	public final AuthType authType;

	public String sessionId;
	public String sessionsName;             // имя поля, в котором нужно передавать sessionId в запросах

	public String socialUserId;             // id юзера в соцсети (для FB, VK)
	public String socialAccessToken;        // accessToken в соцсети (для FB, VK)

	public User user;

	public AuthData() {
		this.authType = null;
	}

	public AuthData(AuthType authType) {
		this.authType = authType;
	}

}