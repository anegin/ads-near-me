package com.adsnearme.app.model;

public class NoError extends Throwable {

	public static final NoError INSTANCE = new NoError();

	private NoError() {
	}

}