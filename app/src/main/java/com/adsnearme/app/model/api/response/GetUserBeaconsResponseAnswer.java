package com.adsnearme.app.model.api.response;

import com.adsnearme.app.model.api.Beacon;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetUserBeaconsResponseAnswer extends ApiAnswer {

	@SerializedName("u_beacons")
	public List<Beacon> beacons;

}