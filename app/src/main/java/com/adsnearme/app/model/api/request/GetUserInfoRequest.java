package com.adsnearme.app.model.api.request;

import com.adsnearme.app.AdsApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.annotations.SerializedName;

public class GetUserInfoRequest extends ApiRequest {

	public String email;                        // при входе по email/pass
	public String password;                     // при входе по email/pass

	public String name;                         // при входе через соцсеть
	public String surname;                      // при входе через соцсеть

	@SerializedName("auth_device_id")
	public final String authDeviceId;           // deviceId

	@SerializedName("android_device_id")
	public final String firebaseDeviceToken;    // id девайса для пушей

	public GetUserInfoRequest() {
		super(ApiAction.GET_USER_INFO);
		this.authDeviceId = AdsApp.component().deviceId();
		this.firebaseDeviceToken = FirebaseInstanceId.getInstance().getToken();
	}

	/*
	 * email, password - при авторизации по email и паролю
	 *
	 * name, surname (+ from, fbUserId, accessToken) - при авторизации через соцсеть
	 *
	 * auth_device_id - для получения профиля пользователя
	 */

}