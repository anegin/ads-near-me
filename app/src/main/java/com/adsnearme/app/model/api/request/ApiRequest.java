package com.adsnearme.app.model.api.request;

public class ApiRequest {

	public final ApiAction action;

	public ApiRequest(ApiAction action) {
		this.action = action;
	}

}