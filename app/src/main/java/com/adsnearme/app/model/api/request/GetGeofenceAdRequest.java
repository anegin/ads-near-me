package com.adsnearme.app.model.api.request;

import com.google.gson.annotations.SerializedName;

public class GetGeofenceAdRequest extends ApiRequest {

	@SerializedName("beacon_id")
	public String beaconId;

	public GetGeofenceAdRequest(String beaconId) {
		super(ApiAction.GET_BEACONS_OFFERS_GEOFENCING);
		this.beaconId = beaconId;
	}

}