package com.adsnearme.app.model.api.response;

import com.adsnearme.app.model.api.request.ApiAction;
import com.google.gson.annotations.SerializedName;

public abstract class ApiResponse<T extends ApiAnswer> {

	public ApiAction action;                    // название метода

	@SerializedName("answer_type")
	public ApiAnswer.Type answerType;           // "ok" или "err"

	public T answer;

	@SerializedName("session_id")
	public String sessionId;                    // id сессии

	@SerializedName("session_name")
	public String sessionName;                  // "PHPSESSID"

}