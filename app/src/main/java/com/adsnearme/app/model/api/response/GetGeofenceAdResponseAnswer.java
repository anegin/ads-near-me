package com.adsnearme.app.model.api.response;

import com.adsnearme.app.model.api.Ad;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetGeofenceAdResponseAnswer extends ApiAnswer {

	@SerializedName("ad_info")
	public List<Ad> ads;            // тут будет только одно объявление с полями - a_id, a_name, a_price, a_description

	@SerializedName("ad_count")
	public Integer adsCount;

}