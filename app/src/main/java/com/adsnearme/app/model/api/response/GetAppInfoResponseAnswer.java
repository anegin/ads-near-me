package com.adsnearme.app.model.api.response;

import com.adsnearme.app.model.api.ApiCategory;

import java.util.Map;

public class GetAppInfoResponseAnswer extends ApiAnswer {

	public Map<String, ApiCategory> categories;

}