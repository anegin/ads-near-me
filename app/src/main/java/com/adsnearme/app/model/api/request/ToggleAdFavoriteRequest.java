package com.adsnearme.app.model.api.request;

import com.google.gson.annotations.SerializedName;

public class ToggleAdFavoriteRequest extends ApiRequest {

	@SerializedName("ad_id")
	public final String adId;

	public final int active;              // 1, 0

	public ToggleAdFavoriteRequest(String adId, boolean isFavorite) {
		super(ApiAction.MANAGE_FAVORITES);
		this.adId = adId;
		this.active = isFavorite ? 1 : 0;
	}

}