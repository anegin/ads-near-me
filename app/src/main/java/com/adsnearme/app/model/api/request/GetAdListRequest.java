package com.adsnearme.app.model.api.request;

import com.google.gson.annotations.SerializedName;

public class GetAdListRequest extends ApiRequest {

	public double latitude;

	public double longitude;

	public int radius;

	@SerializedName("category_id")
	public String categoryId;           // необязательно

	public Integer offset;

	public Integer limit;

	public GetAdListRequest() {
		super(ApiAction.GET_AD_LIST);
	}

}