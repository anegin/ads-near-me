package com.adsnearme.app.model.api.request;

import android.support.annotation.NonNull;

import com.adsnearme.app.model.api.Beacon;
import com.google.gson.annotations.SerializedName;

public class UpdateBeaconRequest extends ApiRequest {

	@SerializedName("beacon_id")
	public String beaconId;                 // обязательно

	public double latitude;                 // обязательно

	public double longitude;                // обязательно

	public String color;                    // "#rrggbb"

	public String name;

	public Beacon.Type type;

	@SerializedName("geofencing_radius")
	public Double geofencingRadius;         // в милях

	public UpdateBeaconRequest(@NonNull Beacon beacon) {
		super(ApiAction.MANAGE_BEACONS);
		this.beaconId = beacon.id;
		this.latitude = beacon.latitude != null ? beacon.latitude : 0;
		this.longitude = beacon.longitude != null ? beacon.longitude : 0;
		this.color = beacon.color;
		this.name = beacon.name;
		this.type = beacon.type;
		this.geofencingRadius = beacon.geofenceRadius;

		// fix lat/long valid values
		if (this.latitude < -90 || this.latitude > 90 || this.longitude < -180 || this.longitude > 180
				|| this.latitude == 0 || this.longitude == 0) {
			// moscow city
			this.latitude = 55.7503;
			this.longitude = 37.6728;
		}
	}

}