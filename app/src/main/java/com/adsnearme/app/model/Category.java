package com.adsnearme.app.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = Category.TABLE_NAME)
public class Category {

	public static final String TABLE_NAME = "categories";

	@NonNull
	@PrimaryKey
	public String id = "";

	@ColumnInfo(name = "parent_id")
	public String parentId;

	public String name;

	@ColumnInfo(name = "additional_filters")
	public boolean additionalFilters;

	public Category() {
	}

	@Ignore
	Category(@NonNull String id, String parentId, String name, boolean additionalFilters) {
		this.id = id;
		this.parentId = parentId;
		this.name = name;
		this.additionalFilters = additionalFilters;
	}

}