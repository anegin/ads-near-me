package com.adsnearme.app.model;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.model.api.ApiCategory;
import com.adsnearme.app.model.api.ApiSubCategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Categories {

	private final Map<String, Category> categoriesMap = new HashMap<>();

	public Categories() {
		AdsApp.component().database().categories()
				.getCategories()
				.observeForever(categories -> {
					synchronized (categoriesMap) {
						categoriesMap.clear();
						if (categories != null) {
							for (Category category : categories) {
								categoriesMap.put(category.id, category);
							}
						}
					}
				});
	}

	public Category getCategory(String id) {
		synchronized (categoriesMap) {
			return categoriesMap.get(id);
		}
	}

	public void updateCategories(Map<String, ApiCategory> apiCategories) {
		if (apiCategories == null) return;

		List<Category> categories = new ArrayList<>();
		for (ApiCategory apiCategory : apiCategories.values()) {
			if (apiCategory != null) {
				String categoryId = apiCategory.id != null ? apiCategory.id : "null_category_id";
				Category category = new Category(categoryId, null, apiCategory.name, apiCategory.additionalFilters);
				categories.add(category);

				if (apiCategory.subcategories != null) {
					for (ApiSubCategory apiSubCategory : apiCategory.subcategories.values()) {
						if (apiSubCategory != null) {
							String subCategoryId = apiSubCategory.id != null ? apiSubCategory.id : "null_subcategory_id";
							Category subCategory = new Category(subCategoryId, categoryId, apiSubCategory.name, apiSubCategory.additionalFilters);
							categories.add(subCategory);
						}
					}
				}
			}
		}
		AdsApp.component().database().categories()
				.setCategories(categories);
	}

}