package com.adsnearme.app.arch.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.api.Api;
import com.adsnearme.app.db.BeaconsDao;
import com.adsnearme.app.model.NoError;
import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.model.api.response.ApiException;

import java.util.List;

import io.reactivex.schedulers.Schedulers;

public class MyBeaconsViewModel extends ViewModel {

	private final LiveData<List<Beacon>> beaconsLiveData;
	private final MutableLiveData<Throwable> errorLiveData = new MutableLiveData<>();

	private boolean isLoadedFromServer;

	public MyBeaconsViewModel() {
		beaconsLiveData = AdsApp.component().database().beacons().getBeacons();
		errorLiveData.setValue(NoError.INSTANCE);
	}

	public LiveData<List<Beacon>> getBeacons() {
		return beaconsLiveData;
	}

	public LiveData<Throwable> getError() {
		return errorLiveData;
	}

	public boolean isLoadedFromServer() {
		return isLoadedFromServer;
	}

	public void setIsLoadedFromServer(boolean isLoadedFromServer) {
		this.isLoadedFromServer = isLoadedFromServer;
	}

	public void loadFromServer() {
		Api.getUserBeacons()
				.observeOn(Schedulers.newThread())
				.subscribe(response -> {
					isLoadedFromServer = true;
					errorLiveData.postValue(NoError.INSTANCE);

					BeaconsDao beaconsDao = AdsApp.component().database().beacons();
					beaconsDao.deleteAll();
					beaconsDao.addBeacons(response.answer.beacons);
				}, t -> {
					if (t instanceof ApiException) {
						ApiException apiException = (ApiException) t;
						if (ApiException.USER_HAVE_NO_BEACONS.equals(apiException.code)) {
							AdsApp.component().database().beacons().deleteAll();
							isLoadedFromServer = true;
							errorLiveData.postValue(NoError.INSTANCE);
							return;
						}
					}
					isLoadedFromServer = false;
					errorLiveData.postValue(t);
				});
	}

}