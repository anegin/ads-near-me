package com.adsnearme.app.arch.paging;

import android.arch.paging.DataSource;
import android.arch.paging.TiledDataSource;

import com.adsnearme.app.api.ApiHelper;
import com.adsnearme.app.model.api.Ad;
import com.adsnearme.app.model.api.request.GetAdListRequest;
import com.adsnearme.app.model.api.response.ApiException;
import com.adsnearme.app.model.api.response.GetAdListResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AdsDataSource extends TiledDataSource<Ad> {

	private final GetAdListRequest request;

	private Integer adsCount;

	public AdsDataSource(GetAdListRequest request) {
		this.request = request;
	}

	@Override
	public int countItems() {
		return adsCount != null ? adsCount : DataSource.COUNT_UNDEFINED;
	}

	@Override
	public List<Ad> loadRange(int startPosition, int count) {
		request.offset = startPosition;
		request.limit = count;
		List<Ad> ads = new ArrayList<>();
		try {
			GetAdListResponse response = ApiHelper.execute(request, GetAdListResponse.class);
			if ((startPosition == 0 || adsCount == null) && response.answer.adsCount != null) {
				adsCount = response.answer.adsCount;
			}
			if (response.answer.ads != null) {
				ads.addAll(response.answer.ads);
			}
		} catch (ApiException e) {
			if (ApiException.EMPTY_LIST.equals(e.code)) {
				// "error_code":"00031","error_text":"Ничего не найдено."
				return ads;
			}
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ads;
	}

}