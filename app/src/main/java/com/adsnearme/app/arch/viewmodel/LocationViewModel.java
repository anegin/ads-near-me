package com.adsnearme.app.arch.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

import com.adsnearme.app.arch.livedata.LocationLiveData;

public class LocationViewModel extends AndroidViewModel {

	private final LocationLiveData locationLiveData;

	public LocationViewModel(Application application) {
		super(application);
		locationLiveData = new LocationLiveData(application);
	}

	public LocationLiveData getLocation() {
		return locationLiveData;
	}

	public void startLocationUpdates() {
		locationLiveData.startLocationUpdates();
	}

}