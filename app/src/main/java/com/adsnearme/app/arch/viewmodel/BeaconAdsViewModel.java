package com.adsnearme.app.arch.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.api.Api;
import com.adsnearme.app.db.AdsDao;
import com.adsnearme.app.model.NoError;
import com.adsnearme.app.model.api.Ad;
import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.model.api.response.ApiException;

import java.util.List;

import io.reactivex.schedulers.Schedulers;

public class BeaconAdsViewModel extends ViewModel {

	private LiveData<List<Ad>> adsLiveData;
	private final MutableLiveData<Throwable> errorLiveData = new MutableLiveData<>();

	private boolean isLoadedFromServer;

	public BeaconAdsViewModel() {
		errorLiveData.setValue(NoError.INSTANCE);
	}

	public LiveData<List<Ad>> getBeaconAds(String beaconId) {
		if (adsLiveData == null) {
			adsLiveData = AdsApp.component().database().ads().getBeaconAds(beaconId);
		}
		return adsLiveData;
	}

	public LiveData<Throwable> getError() {
		return errorLiveData;
	}

	public boolean isLoadedFromServer() {
		return isLoadedFromServer;
	}

	public void setIsLoadedFromServer(boolean isLoadedFromServer) {
		this.isLoadedFromServer = isLoadedFromServer;
	}

	public void loadFromServer(Beacon beacon) {
		Api.getBeaconAds(beacon)
				.observeOn(Schedulers.newThread())
				.subscribe(response -> {
					isLoadedFromServer = true;
					errorLiveData.postValue(NoError.INSTANCE);

					for (Ad ad : response.answer.ads) {
						ad.beaconId = beacon.id;
					}

					AdsDao adsDao = AdsApp.component().database().ads();
					adsDao.addAds(response.answer.ads);
				}, t -> {
					if (t instanceof ApiException) {
						ApiException apiException = (ApiException) t;
						if (ApiException.EMPTY_LIST.equals(apiException.code)) {
							// "error_code":"00031","error_text":"Такого бикона не существует или он не активен."
							// "error_code":"00031","error_text":"Объявлений по фильтру не найдено."

							// удаляем все объявления для этого бикона
							AdsApp.component().database().ads().deleteBeaconAds(beacon.id);

							isLoadedFromServer = true;
							errorLiveData.postValue(NoError.INSTANCE);
							return;
						}
					}
					isLoadedFromServer = false;
					errorLiveData.postValue(t);
				});
	}

}