package com.adsnearme.app.arch.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListProvider;
import android.arch.paging.PagedList;

import com.adsnearme.app.arch.paging.AdsDataSource;
import com.adsnearme.app.model.api.Ad;
import com.adsnearme.app.model.api.request.GetAdListRequest;
import com.adsnearme.app.utils.Utils;

public class AdsListViewModel extends ViewModel {

	public static final int[] RADIUSES = new int[]{
			500, 1000, 5000, 10000, 15000, 20000
	};

	private final MutableLiveData<Integer> radiusIndex = new MutableLiveData<>();

	private boolean distanceInMeters;
	private final MutableLiveData<Boolean> isMapMode = new MutableLiveData<>();

	private final MutableLiveData<GetAdListRequest> requestLiveData = new MutableLiveData<>();
	private LiveData<PagedList<Ad>> adsLiveData;

	public AdsListViewModel() {
		radiusIndex.setValue(0);
		isMapMode.postValue(false);
		distanceInMeters = true;
	}

	public boolean isRequested() {
		return requestLiveData.getValue() != null;
	}

	public void setRequest(GetAdListRequest request) {
		requestLiveData.postValue(request);
		adsLiveData = new LivePagedListProvider<Integer, Ad>() {
			@Override
			protected DataSource<Integer, Ad> createDataSource() {
				return new AdsDataSource(request);
			}
		}.create(0, new PagedList.Config.Builder()
				.setPageSize(20)
				.setEnablePlaceholders(false)
				.build());
	}

	public LiveData<GetAdListRequest> getRequest() {
		return requestLiveData;
	}

	public void removeObserver(Observer<PagedList<Ad>> observer) {
		if (adsLiveData != null) {
			adsLiveData.removeObserver(observer);
		}
	}

	public LiveData<PagedList<Ad>> getAds() {
		return adsLiveData;
	}

	public void setDistanceInMeters(boolean distanceInMeters) {
		this.distanceInMeters = distanceInMeters;
	}

	public boolean isDistanceInMeters() {
		return distanceInMeters;
	}

	public LiveData<Boolean> getIsMapMode() {
		return isMapMode;
	}

	public void toggleIsMapMode() {
		Boolean isMapMode = this.isMapMode.getValue();
		this.isMapMode.postValue(isMapMode == null || !isMapMode);
	}

	public void increaseRadius() {
		int index = radiusIndex.getValue() != null ? radiusIndex.getValue() : 0;
		if (index + 1 == RADIUSES.length) return;
		radiusIndex.postValue(index + 1);
	}

	public void decreaseRadius() {
		int index = radiusIndex.getValue() != null ? radiusIndex.getValue() : 0;
		if (index == 0) return;
		radiusIndex.postValue(index - 1);
	}

	public LiveData<Integer> getRadiusIndex() {
		return radiusIndex;
	}

	public int getRadius() {
		Integer index = radiusIndex.getValue();
		return RADIUSES[index != null ? index : 0];
	}

	public boolean toggleAdFavorite(String adId, boolean isFavorite) {
		PagedList<Ad> pagedList = adsLiveData.getValue();
		if (pagedList == null) return false;
		for (Ad ad : pagedList) {
			if (ad != null && Utils.objectEquals(ad.id, adId)) {
				ad.inFavorites = isFavorite ? 1 : 0;
				return true;
			}
		}
		return false;
	}

}