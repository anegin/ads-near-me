package com.adsnearme.app.arch.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class EditGeofenceRadiusViewModel extends ViewModel {

	private final MutableLiveData<Integer> radius = new MutableLiveData<>();

	public void setRadius(int radius) {
		this.radius.postValue(radius);
	}

	public LiveData<Integer> getRadius() {
		return radius;
	}

}