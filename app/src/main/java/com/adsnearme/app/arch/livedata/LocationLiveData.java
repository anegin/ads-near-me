package com.adsnearme.app.arch.livedata;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.location.Location;

import com.adsnearme.app.utils.LocationHelper;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class LocationLiveData extends LiveData<Location> {

	private FusedLocationProviderClient fusedLocationClient;

	private boolean requestingLocationUpdates;

	public LocationLiveData(Context context) {
		fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
	}

	public void startLocationUpdates() {
		if (!requestingLocationUpdates) {
			LocationRequest locationRequest = new LocationRequest();
			locationRequest.setInterval(10000);
			locationRequest.setFastestInterval(5000);
			locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

			try {
				fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
				requestingLocationUpdates = true;
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onActive() {
		if (LocationHelper.hasPermission()) {
			startLocationUpdates();
			try {
				fusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
					if (location != null) postValue(location);
				});
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onInactive() {
		if (requestingLocationUpdates) {
			fusedLocationClient.removeLocationUpdates(locationCallback);
		}
	}

	private LocationCallback locationCallback = new LocationCallback() {
		@Override
		public void onLocationResult(LocationResult locationResult) {
			Location location = locationResult.getLastLocation();
			if (location != null) {
				postValue(location);
			}
		}
	};

}