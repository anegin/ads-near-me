package com.adsnearme.app.arch.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.adsnearme.app.AdsApp;
import com.adsnearme.app.api.Api;
import com.adsnearme.app.model.api.Beacon;
import com.adsnearme.app.model.api.request.UpdateBeaconRequest;

import io.reactivex.schedulers.Schedulers;

public class ViewBeaconViewModel extends ViewModel {

	private LiveData<Beacon> beaconLiveData;

	private final MutableLiveData<Boolean> isInEditNameMode = new MutableLiveData<>();

	public ViewBeaconViewModel() {
		isInEditNameMode.setValue(false);
	}

	public LiveData<Beacon> getBeacon(String beaconId) {
		if (beaconLiveData == null) {
			beaconLiveData = AdsApp.component().database().beacons().getBeacon(beaconId);
		}
		return beaconLiveData;
	}

	public LiveData<Beacon> getBeacon() {
		return beaconLiveData;
	}

	public Beacon getBeaconValue() {
		return beaconLiveData != null ? beaconLiveData.getValue() : null;
	}

	public void setBeaconType(Beacon beacon, Beacon.Type newType) {
		UpdateBeaconRequest request = new UpdateBeaconRequest(beacon);
		request.type = newType;
		Api.updateBeacon(request)
				.observeOn(Schedulers.single())
				.subscribe(updateBeaconResponse -> {
					beacon.type = newType;
					AdsApp.component().database().beacons().updateBeacon(beacon);
				}, throwable -> {
					// "error_code":"00013","error_text":"Недопустимый id бикона."
					// "error_code":"00029","error_text":"Неверное значение latitude."
					// "error_code":"00030","error_text":"Неверное значение longitude."
					AdsApp.component().database().beacons().updateBeacon(beacon);
				});
	}

	public LiveData<Boolean> getIsInEditNameMode() {
		return isInEditNameMode;
	}

	public void setIsInEditNameMode(boolean inEditNameMode) {
		isInEditNameMode.postValue(inEditNameMode);
	}

	public boolean isInEditNameMode() {
		Boolean inEditMode = isInEditNameMode.getValue();
		return inEditMode != null ? inEditMode : false;
	}

	public void setBeaconName(Beacon beacon, String newName) {
		UpdateBeaconRequest request = new UpdateBeaconRequest(beacon);
		request.name = newName;
		Api.updateBeacon(request)
				.observeOn(Schedulers.single())
				.subscribe(updateBeaconResponse -> {
					beacon.name = newName;
					AdsApp.component().database().beacons().updateBeacon(beacon);
				}, throwable -> {
					// "error_code":"00013","error_text":"Недопустимый id бикона."
					// "error_code":"00029","error_text":"Неверное значение latitude."
					// "error_code":"00030","error_text":"Неверное значение longitude."
					AdsApp.component().database().beacons().updateBeacon(beacon);
				});
	}

	public void setBeaconRadius(Beacon beacon, int newRadius) {
		UpdateBeaconRequest request = new UpdateBeaconRequest(beacon);
		request.geofencingRadius = (double) newRadius;
		Api.updateBeacon(request)
				.observeOn(Schedulers.single())
				.subscribe(updateBeaconResponse -> {
					beacon.geofenceRadius = (double) newRadius;
					AdsApp.component().database().beacons().updateBeacon(beacon);
				}, throwable -> {
					// "error_code":"00013","error_text":"Недопустимый id бикона."
					// "error_code":"00029","error_text":"Неверное значение latitude."
					// "error_code":"00030","error_text":"Неверное значение longitude."
					AdsApp.component().database().beacons()
							.updateBeacon(beacon);
				});
	}

}