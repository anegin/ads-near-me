package com.adsnearme.app;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDexApplication;

import com.adsnearme.app.di.AppComponent;
import com.adsnearme.app.di.AppModule;
import com.adsnearme.app.di.DaggerAppComponent;
import com.adsnearme.app.geofence.UpdateGeofencesJob;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;
import com.evernote.android.job.JobManager;

import net.danlew.android.joda.JodaTimeAndroid;

public class AdsApp extends MultiDexApplication {

	private static AppComponent appComponent;

	@Override
	public void onCreate() {
		super.onCreate();
		appComponent = DaggerAppComponent.builder()
				.appModule(new AppModule(this))
				.build();

		JodaTimeAndroid.init(this);

		JobManager.create(this).addJobCreator(new AdsJobCreator());
		UpdateGeofencesJob.schedule();
	}

	public static AppComponent component() {
		return appComponent;
	}

	// =============

	private static class AdsJobCreator implements JobCreator {

		@Nullable
		@Override
		public Job create(@NonNull String tag) {
			switch (tag) {
				case UpdateGeofencesJob.JOB_TAG:
					return new UpdateGeofencesJob();
			}
			return null;
		}

	}

}