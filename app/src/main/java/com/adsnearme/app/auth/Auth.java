package com.adsnearme.app.auth;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.adsnearme.app.model.AuthData;
import com.adsnearme.app.utils.GsonUtils;
import com.google.gson.JsonSyntaxException;

import com.adsnearme.app.AdsApp;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

public class Auth {

	private static final String PREF_AUTH_DATA = "auth_data";

	private final BehaviorSubject<AuthData> sessionSubj = BehaviorSubject.create();

	public Auth() {
		sessionSubj.onNext(loadFromPreferences());
		sessionSubj
				.subscribeOn(Schedulers.single())
				.subscribe(Auth::saveToPreferences);
	}

	public Observable<AuthData> observer() {
		return sessionSubj;
	}

	public void setAuthData(AuthData authData) {
		sessionSubj.onNext(authData != null ? authData : new AuthData());
	}

	@NonNull
	public AuthData getAuthData() {
		return sessionSubj.getValue();
	}

	@NonNull
	private static AuthData loadFromPreferences() {
		SharedPreferences prefs = AdsApp.component().prefs();
		String authDataJson = prefs.getString(PREF_AUTH_DATA, null);
		if (authDataJson != null) {
			try {
				return GsonUtils.GSON.fromJson(authDataJson, AuthData.class);
			} catch (JsonSyntaxException ignored) {
				prefs.edit().remove(PREF_AUTH_DATA).apply();
			}
		}
		return new AuthData();
	}

	private static void saveToPreferences(@Nullable AuthData authData) {
		SharedPreferences prefs = AdsApp.component().prefs();
		if (authData == null) {
			prefs.edit().remove(PREF_AUTH_DATA).apply();
		} else {
			String authDataJson = GsonUtils.GSON.toJson(authData);
			prefs.edit().putString(PREF_AUTH_DATA, authDataJson).apply();
		}
	}

}