package com.adsnearme.app.auth;

public enum AuthType {

	EMAIL(null),
	FACEBOOK("fb"),
	VKONTAKTE("vk");

	public final String value;

	AuthType(String value) {
		this.value = value;
	}

}